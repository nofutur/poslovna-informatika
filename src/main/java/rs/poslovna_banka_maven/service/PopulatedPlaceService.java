package rs.poslovna_banka_maven.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.poslovna_banka_maven.entity.PopulatedPlace;
import rs.poslovna_banka_maven.repository.PopulatedPlaceRepository;


@Service
public class PopulatedPlaceService implements PopulatedPlaceInterface{
	@Autowired
	private PopulatedPlaceRepository populatedPlaceRepository;
	
	@Override
    public List<PopulatedPlace> getAll() {
		return populatedPlaceRepository.findAll();
    }
	
	@Override
    public PopulatedPlace get(Integer id) {
    	Optional<PopulatedPlace> populatedPlace = populatedPlaceRepository.findById(id);
        return populatedPlace.get();
    }
	
	@Override
    public PopulatedPlace insert(PopulatedPlace populatedPlace) {
    	return populatedPlaceRepository.save(populatedPlace);
    }
	
	@Override
    public PopulatedPlace edit(PopulatedPlace populatedPlace) {
        return populatedPlaceRepository.save(populatedPlace);
    }
	
	@Override
    public void delete(Integer id) {
		populatedPlaceRepository.deleteById(id);
    }
}