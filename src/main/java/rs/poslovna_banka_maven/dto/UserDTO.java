package rs.poslovna_banka_maven.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import rs.poslovna_banka_maven.entity.BankAccount;
import rs.poslovna_banka_maven.entity.User;

public class UserDTO implements Serializable {
	private int id;
	@NotBlank(message = "Ne moze bez usernama")
	private String username;
	@NotBlank(message = "Ne moze bez adrese")
	private String address;
	@NotBlank(message = "Ne moze bez id broja")
	@Pattern(regexp = "^[0-9]*$")
	private String identificationNumber;
	@NotBlank(message = "Ne moze bez broja telefona")
	@Pattern(regexp = "^[0-9]*$")
	private String phoneNumber;
	@NotBlank(message = "Ne moze bez imena")
	private String firstname;
	@NotBlank(message = "Ne moze bez prezimena")
	private String lastname;
	private boolean legalEntity;
	List<BankAccountDTO> bankAccounts;

	public UserDTO() {}

	public UserDTO(int id, String username, String address, String identificationNumber,
			String phoneNumber, List<BankAccountDTO> bankAccounts, 
			String firstname, String lastname, boolean legalEntity ) {
		super();
		this.id = id;
		this.username = username;
		this.address = address;
		this.identificationNumber = identificationNumber;
		this.phoneNumber = phoneNumber;
		this.bankAccounts = bankAccounts;
		this.firstname = firstname;
		this.lastname = lastname;
		this.legalEntity = legalEntity;
	}

	public UserDTO(User user) {
		this.setId(user.getId());
		this.setUsername(user.getUsername());
		this.setAddress(user.getAddress());
		this.setIdentificationNumber(user.getIdentificationNumber());
		this.setPhoneNumber(user.getPhoneNumber());
		
		this.setFirstname(user.getFirstname());
		this.setLastname(user.getLastname());
		this.setLegalEntity(user.getLegalEntity());
		
		this.bankAccounts = new ArrayList<BankAccountDTO>();
		
		if (user.getBankAccounts() != null) {
			for (BankAccount bankAccount : user.getBankAccounts()) {
				bankAccounts.add(new BankAccountDTO(bankAccount));
			}
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<BankAccountDTO> getBankAccounts() {
		return bankAccounts;
	}

	public void setBankAccounts(List<BankAccountDTO> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public boolean isLegalEntity() {
		return legalEntity;
	}

	public void setLegalEntity(boolean legalEntity) {
		this.legalEntity = legalEntity;
	}

	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", username=" + username + ", address=" + address + ", identificationNumber="
				+ identificationNumber + ", phoneNumber=" + phoneNumber + ", firstname=" + firstname + ", lastname="
				+ lastname + ", legalEntity=" + legalEntity + ", bankAccounts=" + bankAccounts + "]";
	}
	
	

}
