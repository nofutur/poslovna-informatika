package rs.poslovna_banka_maven.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import rs.poslovna_banka_maven.dto.CurrencyDTO;
import rs.poslovna_banka_maven.dto.TransakcijeDTO;

@Entity
@Table(name="Transactions")
public class Transakcije implements Serializable {
	
	public enum Error{
		error_with_server
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id",unique = true,nullable = false)
	private int id;
	
	@OneToOne
	@JoinColumn(name="bank_account_id_debtor", referencedColumnName="bank_account_id", nullable=false)
	private BankAccount debtor;
	
	@Column(name = "purpose_of_payment",nullable = false, length = 100)
	private String purposeOfPayment;
	
	@Column(name = "creditor",nullable = false, length = 80)
	private String creditor;
	
	@Column(name = "date_of_receipt",nullable = false)
	private Date dateOfReceipt;
	
	@Column(name = "date_of_value",nullable = false)
	private Date dateOfValue;
	
	@Column(name="debtor_account", nullable=false)
	private String debtorAccount;
	
	@Column(name = "debit_model",nullable = false)
	private int debitModel;
	
	@Column(name = "call_number",nullable = false)
	private int callNumber;
	
	@Column(name = "urgently" ,nullable = false)
	private boolean urgently;
	
	@Column(name = "amount",nullable = false)
	private int amount;
	
	@Column(name = "error",nullable = true)
	private Error error;
	
	@Column(name = "status" ,nullable = false)
	private boolean status;
	
	@OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.LAZY )
	private Set<Currency> currencies = new HashSet<Currency>();
	
	@ManyToOne
	@JoinColumn(name="daily_condition_id", referencedColumnName="daily_condition_id", nullable=false)
	private DailyCondition dailyCondition;
	
	@ManyToOne
	@JoinColumn(name="message_id", referencedColumnName="message_id", nullable=false)
	private Message message;
	

	@ManyToOne
	@JoinColumn(name="populated_place_id", referencedColumnName="id", nullable=false)
	private PopulatedPlace place;
	
	@ManyToOne
	@JoinColumn(name="type_of_payment_id", referencedColumnName="type_of_payment_id", nullable=false)
	private TypesOfPayments typeOfPayments ;


	public Transakcije() {
		super();
	}

//	public Transakcije(TransakcijeDTO t) {
//		super();
//		this.id = t.getId();
//		this.debtor = t.getDebtor();
//		this.purposeOfPayment = t.getPurposeOfPayment();
//		this.creditor = t.getCreditor();
//		this.dateOfReceipt = t.getDateOfReceipt();
//		this.dateOfValue = t.getDateOfValue();
//		this.debtorAccount = t.getDebtorAccount();
//		this.debitModel = t.getDebitModel();
//		this.callNumber = t.getCallNumber();
//		this.urgently = t.isUrgently();
//		this.amount = t.getAmount();
//		this.error = t.getError();
//		this.status = t.isStatus();
//	 
//		if(t.getCurrencies() != null) {
//			for (CurrencyDTO c : t.getCurrencies()) {
//				this.currencies.add(new Currency(c));
//			}
//		}
//		this.dailyCondition=t.getDailyCondition();
//		this.message=t.getMessage();
//		this.place=t.getPlace();
//		this.typeOfPayments=t.getTypeOfPayments();
//		
//	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BankAccount getDebtor() {
		return debtor;
	}

	public void setDebtor(BankAccount debtor) {
		this.debtor = debtor;
	}

	public String getPurposeOfPayment() {
		return purposeOfPayment;
	}

	public void setPurposeOfPayment(String purposeOfPayment) {
		this.purposeOfPayment = purposeOfPayment;
	}

	public String getCreditor() {
		return creditor;
	}

	public void setCreditor(String creditor) {
		this.creditor = creditor;
	}

	public Date getDateOfReceipt() {
		return dateOfReceipt;
	}

	public void setDateOfReceipt(Date dateOfReceipt) {
		this.dateOfReceipt = dateOfReceipt;
	}

	public Date getDateOfValue() {
		return dateOfValue;
	}

	public void setDateOfValue(Date dateOfValue) {
		this.dateOfValue = dateOfValue;
	}

	public String getDebtorAccount() {
		return debtorAccount;
	}

	public void setDebtorAccount(String debtorAccount) {
		this.debtorAccount = debtorAccount;
	}

	public int getDebitModel() {
		return debitModel;
	}

	public void setDebitModel(int debitModel) {
		this.debitModel = debitModel;
	}

	public int getCallNumber() {
		return callNumber;
	}

	public void setCallNumber(int callNumber) {
		this.callNumber = callNumber;
	}

	public boolean isUrgently() {
		return urgently;
	}

	public void setUrgently(boolean urgently) {
		this.urgently = urgently;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Set<Currency> getCurrencies() {
		return currencies;
	}

	public void setCurrencies(Set<Currency> currencies) {
		this.currencies = currencies;
	}

	

	public DailyCondition getDailyCondition() {
		return dailyCondition;
	}

	public void setDailyCondition(DailyCondition dailyCondition) {
		this.dailyCondition = dailyCondition;
	}

	public PopulatedPlace getPlace() {
		return place;
	}

	public void setPlace(PopulatedPlace place) {
		this.place = place;
	}

	public TypesOfPayments getTypeOfPayments() {
		return typeOfPayments;
	}

	public void setTypeOfPayments(TypesOfPayments typeOfPayments) {
		this.typeOfPayments = typeOfPayments;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}
	

	
	
	
	
}
