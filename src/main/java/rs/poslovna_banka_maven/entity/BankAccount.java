package rs.poslovna_banka_maven.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.poslovna_banka_maven.dto.BankAccountDTO;
import rs.poslovna_banka_maven.dto.CurrencyDTO;
import rs.poslovna_banka_maven.dto.DailyConditionDTO;

@Entity
@Table(name="bank_accounts")
public class BankAccount {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="bank_account_id")
	private int id;
	
	@Column(name = "number",unique = true,nullable = false, length = 40)
	private String number;
	
	@Column(name = "date_open",nullable = false)
	private Date date;
	
	@Column(name = "balance",nullable = false)
	private double balance;
	
	@Column(name = "valid",nullable = false)
	@ColumnDefault("1")
	private boolean valid;
	

	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="user_id", nullable=false)
	@JsonIgnore
	private User user;
	
	
	@ManyToOne
	@JoinColumn(name="currency_id", referencedColumnName="currency_id", nullable=false)
	@JsonIgnore
	private Currency currency;
	
	@OneToMany(cascade= {ALL}, fetch=LAZY, mappedBy="bankAccount" )
	@JsonIgnore
	private Set<DailyCondition> dailyConditions = new HashSet<DailyCondition>();
	
	@ManyToOne
	@JoinColumn(name="bank_id", referencedColumnName="bank_id", nullable=false)
	@JsonIgnore
	private Bank bank;



	public BankAccount() {}
	
	public BankAccount(BankAccountDTO bankAccount) {
		super();
		this.id = bankAccount.getId();
		this.number = bankAccount.getNumber();
		this.date = bankAccount.getDate();
		this.balance = bankAccount.getBalance();
		this.valid = bankAccount.isValid();
		
		if (bankAccount.getDailyConditions() != null) {
			for (DailyConditionDTO dailyCondition : bankAccount.getDailyConditions()) {
				this.dailyConditions.add(new DailyCondition(dailyCondition));
			}
		}
		
		
	}

	
	public void addDailyCondition(DailyCondition dailyCondition) {
		if (dailyCondition.getBankAccount() != null) {
			dailyCondition.getBankAccount().getDailyConditions().remove(dailyCondition);
		}
		dailyCondition.setBankAccount(this);
		getDailyConditions().add(dailyCondition);
	}
	
	public void removeDailyCondition(DailyCondition dailyCondition) {
		dailyCondition.setBankAccount(null);
		getDailyConditions().remove(dailyCondition);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Set<DailyCondition> getDailyConditions() {
		return dailyConditions;
	}

	public void setDailyConditions(Set<DailyCondition> dailyConditions) {
		this.dailyConditions = dailyConditions;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public double getNewAmur(double amount) {
		double newA=this.balance+amount;
		return newA;
	}
	
	

}
