package rs.poslovna_banka_maven.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import rs.poslovna_banka_maven.entity.BankAccount;
import rs.poslovna_banka_maven.entity.Country;
import rs.poslovna_banka_maven.entity.Currency;
import rs.poslovna_banka_maven.entity.Transakcije;

public class CurrencyDTO {

	private int id;
	
	private String codeOfCurrency;
	
	private String name;
	
	private ArrayList<TransakcijeDTO> transactions;

	private ArrayList<CountryDTO> countries;

	public CurrencyDTO() {
		super();
	}

	public CurrencyDTO(Currency c) {
		super();
		this.id = c.getId();
		this.codeOfCurrency = c.getCodeOfCurrency();
		this.name = c.getName();
		ArrayList<TransakcijeDTO>sub=new ArrayList<TransakcijeDTO>(); 
		if(c.getTransactions() != null) {
			for (Transakcije s : c.getTransactions()) {
				sub.add(new TransakcijeDTO(s));
			}
		}
		this.transactions=sub;

		ArrayList<BankAccountDTO>bankADtos=new ArrayList<BankAccountDTO>(); 
	
		ArrayList<CountryDTO>cou=new ArrayList<CountryDTO>(); 
		if(c.getCountries() != null) {
			for (Country co :c.getCountries() ) {
				cou.add(new CountryDTO(co));
			}
		}
		this.countries=cou;

	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodeOfCurrency() {
		return codeOfCurrency;
	}

	public void setCodeOfCurrency(String codeOfCurrency) {
		this.codeOfCurrency = codeOfCurrency;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<TransakcijeDTO> getTransactions() {
		return transactions;
	}

	public void setTransactions(ArrayList<TransakcijeDTO> transactions) {
		this.transactions = transactions;
	}

	public ArrayList<CountryDTO> getCountries() {
		return countries;
	}

	public void setCountries(ArrayList<CountryDTO> countries) {
		this.countries = countries;
	}
	
	
	
}
