package rs.poslovna_banka_maven.service;

import rs.poslovna_banka_maven.entity.Currency;

public interface CurrencyServiceInterface {
	
	Currency findByCodeOfCurrency(String codeOfCurrency);
}
