package rs.poslovna_banka_maven.service;

import java.util.List;

import rs.poslovna_banka_maven.dto.*;


public interface BankServiceInterface {

	public List<BankDTO> findAll();
	public BankDTO findByUsername(String usernameboolean );
}
