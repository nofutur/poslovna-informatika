package rs.poslovna_banka_maven.dto;

import rs.poslovna_banka_maven.entity.Country;
import rs.poslovna_banka_maven.entity.Currency;

public class CountryDTO {
	private int id;
	private String code;
	private String name;
	private Currency currency;
	
	public CountryDTO() {
		super();
	}
	
	public CountryDTO(Country c) {
		super();
		this.id = c.getId();
		this.code = c.getCode();
		this.name = c.getName();
		this.currency=c.getCurrency();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Currency getCurrency() {
		return currency;
	}
	
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	
	
}
