package rs.poslovna_banka_maven.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.poslovna_banka_maven.dto.CountryDTO;
import rs.poslovna_banka_maven.dto.TypesOfPaymentsDTO;

@Entity
@Table(name = "types_of_payments")
public class TypesOfPayments {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "type_of_payment_id", unique = true, nullable = false)
	private int id;
	
	@Column(name = "payment_type_code", nullable = false)
	private String paymentTypeCode;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@OneToMany(cascade= {ALL}, fetch=LAZY, mappedBy="typeOfPayments")
	@JsonIgnore
	private Set<Transakcije> transactions;

	public TypesOfPayments() {
		super();
	}

	public TypesOfPayments(int id, String paymentTypeCode, String name, Set<Transakcije> transactions) {
		this.id = id;
		this.paymentTypeCode = paymentTypeCode;
		this.name = name;
		this.transactions = transactions;
	}

	public TypesOfPayments(TypesOfPaymentsDTO cDTO) {
		this.id = cDTO.getId();
		this.paymentTypeCode = cDTO.getPaymentTypeCode();
		this.name = cDTO.getName();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPaymentTypeCode() {
		return paymentTypeCode;
	}

	public void setPaymentTypeCode(String paymentTypeCode) {
		this.paymentTypeCode = paymentTypeCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Transakcije> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<Transakcije> transactions) {
		this.transactions = transactions;
	}
}
