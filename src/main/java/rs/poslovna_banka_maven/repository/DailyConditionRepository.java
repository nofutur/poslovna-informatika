package rs.poslovna_banka_maven.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rs.poslovna_banka_maven.entity.DailyCondition;

@Repository
public interface DailyConditionRepository extends JpaRepository<DailyCondition, Integer>{
	public DailyCondition findById(int id);
}
