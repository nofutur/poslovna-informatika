package rs.poslovna_banka_maven.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.poslovna_banka_maven.dto.BankDTO;
import rs.poslovna_banka_maven.dto.ExchangeRateListDTO;

@Entity
@Table(name="exchange_rate_list")
public class ExchangeRateList {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "exchangeRateList_id", unique = true, nullable = false)
	private int id;
	
	@Column(name = "Date")
	private Date date;
	
	@Column(name = "Number_of_exchange_rate_list")
	private int numberOfExchangeRateList;
	
	@Column(name = "Applied_by")
	private Date appliedBy;
	
	@OneToMany(cascade= {ALL}, fetch=LAZY, mappedBy="exchangeRateList")
	@JsonIgnore
	private Set<CurrencyExchangeRate> currencyExchangeRates = new HashSet<CurrencyExchangeRate>();
	
	@ManyToOne
	@JoinColumn(name="bank_id_erl", referencedColumnName="bank_id", nullable=false)
	@JsonIgnore
	Bank bank;

	


	public ExchangeRateList(ExchangeRateListDTO erl) {
		this.id = erl.getId();
		this.date = erl.getDate();
		this.numberOfExchangeRateList = erl.getNumberOfExchangeRateList();
		this.appliedBy = erl.getAppliedBy();
	}
	
	public ExchangeRateList(Date date, int numberOfExchangeRateList, Date appliedBy, Bank bank) {
		this.date = date;
		this.numberOfExchangeRateList = numberOfExchangeRateList;
		this.appliedBy = appliedBy;
		this.bank = bank;
	}

	public ExchangeRateList() {
		
	}


	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getNumberOfExchangeRateList() {
		return numberOfExchangeRateList;
	}

	public void setNumberOfExchangeRateList(int numberOfExchangeRateList) {
		this.numberOfExchangeRateList = numberOfExchangeRateList;
	}

	public Date getAppliedBy() {
		return appliedBy;
	}

	public void setAppliedBy(Date appliedBy) {
		this.appliedBy = appliedBy;
	}

	public Set<CurrencyExchangeRate> getCurrencyExchangeRates() {
		return currencyExchangeRates;
	}

	public void setCurrencyExchangeRates(Set<CurrencyExchangeRate> currencyExchangeRates) {
		this.currencyExchangeRates = currencyExchangeRates;
	}

	public Bank getBank() {
		return bank;
	}
	
	public void setBank(Bank bank) {
		this.bank = bank;
	}
}
