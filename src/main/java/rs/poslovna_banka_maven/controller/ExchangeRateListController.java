package rs.poslovna_banka_maven.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.poslovna_banka_maven.dto.BankDTO;
import rs.poslovna_banka_maven.dto.ERLCreateDTO;
import rs.poslovna_banka_maven.dto.ExchangeRateListDTO;
import rs.poslovna_banka_maven.entity.Bank;
import rs.poslovna_banka_maven.entity.CurrencyExchangeRate;
import rs.poslovna_banka_maven.entity.ExchangeRateList;
import rs.poslovna_banka_maven.service.BankService;
import rs.poslovna_banka_maven.service.BankServiceInterface;
import rs.poslovna_banka_maven.service.ExchangeRateListService;

@RestController
@RequestMapping("api/exchangeRateList")
public class ExchangeRateListController {

	@Autowired
	private ExchangeRateListService erlService;

	@Autowired
	private BankServiceInterface bankService;

	@GetMapping()
	public ResponseEntity<List<ExchangeRateListDTO>> getExchangeRateList() {
		List<ExchangeRateListDTO> erl = erlService.findAll();

		return new ResponseEntity<>(erl, HttpStatus.OK);

	}

	@GetMapping("/{id}")
	public ResponseEntity<ExchangeRateListDTO> getOne(@PathVariable Integer id) {
		ExchangeRateListDTO erl = erlService.getOne(id);
		if (erl == null) {
			return new ResponseEntity<ExchangeRateListDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ExchangeRateListDTO>(erl, HttpStatus.OK);
	}

//	exchangeRateList" + date + "/" + numberOfExchangeRateList"/" + appliedBy + "/" + bank

	@PostMapping(path = "{date1}/{numberOfExchangeRateList1}/{appliedBy1}/{bankconsumes1}")
	public ResponseEntity<ExchangeRateListDTO> insert(
			@PathVariable String date1,
			@PathVariable int numberOfExchangeRateList1,
			@PathVariable String appliedBy1,
			@PathVariable String bankconsumes1)
			throws Exception {

		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd", Locale.ENGLISH);
	
		Date date= formatter.parse(date1);
		Date appliedBy = formatter.parse(appliedBy1);
		
		
		System.err.println("ExchangeRateListDTO...");
		System.err.println("ExchangeRateListDTO..."+date);

		System.err.println("ExchangeRateListDTO..."+appliedBy);

		BankDTO bankDTO = bankService.findByUsername(bankconsumes1);
	
		
		System.err.println("ExchangeRateListDTO..."+bankDTO.getId());

		
		ExchangeRateListDTO erlDTO = new ExchangeRateListDTO();
		erlDTO.setAppliedBy(appliedBy);
		erlDTO.setDate(date);
		
		erlDTO.setBank(bankDTO);
	
	
		erlDTO.setNumberOfExchangeRateList(numberOfExchangeRateList1);

		ExchangeRateListDTO erl = erlService.save(erlDTO);
	
		return new ResponseEntity<ExchangeRateListDTO>(erl, HttpStatus.OK);	
		
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id){
		ExchangeRateList erl = erlService.get(id);
		System.out.println(erl);
		if (erl != null){
			erlService.delete(id);
			System.out.println("obrisano");
			return new ResponseEntity<Void>(HttpStatus.OK);
			
		} else {		
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}


}
