package rs.poslovna_banka_maven.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rs.poslovna_banka_maven.entity.Bank;

@Repository
public interface BankRepository extends JpaRepository<Bank, Integer> {

	
	Bank findByUsername(String username);

}
