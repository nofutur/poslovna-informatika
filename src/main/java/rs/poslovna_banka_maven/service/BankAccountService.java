package rs.poslovna_banka_maven.service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import rs.poslovna_banka_maven.dto.BankAccountDTO;
import rs.poslovna_banka_maven.dto.BankDTO;
import rs.poslovna_banka_maven.dto.CurrencyDTO;
import rs.poslovna_banka_maven.dto.UserDTO;
import rs.poslovna_banka_maven.entity.Bank;
import rs.poslovna_banka_maven.entity.BankAccount;
import rs.poslovna_banka_maven.entity.BankAccountClose;
import rs.poslovna_banka_maven.entity.Currency;
import rs.poslovna_banka_maven.entity.User;
import rs.poslovna_banka_maven.repository.BankAccountCloseReposytory;
import rs.poslovna_banka_maven.repository.BankAccountRepository;

@Service
public class BankAccountService implements BankAccountServiceInterface {

	@Autowired
	private BankAccountRepository bar;
	
	
	@Autowired
	private BankAccountCloseReposytory bankAccountCloseReposytory;
	

	@Override
	public List<BankAccountDTO> findByUser(UserDTO user) {
		List<BankAccount> ba = bar.findByUser(new User(user));
	
		List<BankAccountDTO> badto = new ArrayList<>();
	
		for(BankAccount b : ba) {
			BankAccountDTO bd =new BankAccountDTO(b);
			bd.setUser(user);
			bd.setBank(new BankDTO(b.getBank()));
			badto.add(bd);
		}
		return badto;
	}


	

	
	@Override
	public BankAccountDTO  findByNumber(String number) {
	BankAccount bankAccount=bar.findByNumber(number);
	
	if(bankAccount==null) {
		return null;
	}
	
		BankAccountDTO bankAccountDTO = new BankAccountDTO(bankAccount);
//		bankAccountDTO.setBalance(bankAccount.getBalance());
////		bankAccountDTO.setCurrency(bankAccount.getCurrency());
////		bankAccountDTO.setDailyConditions(bankAccount.getDailyConditions());
//		bankAccountDTO.setDate(bankAccount.getDate());
//		bankAccountDTO.setId(bankAccount.getId());
//		bankAccountDTO.setNumber(bankAccount.getNumber());
		bankAccountDTO.setUser(new UserDTO(bankAccount.getUser()));
		bankAccountDTO.setBank(new BankDTO(bankAccount.getBank()));
		return bankAccountDTO;
	}


	@Override
	public void save(BankAccount ba) {
		System.out.println("saving...");
		bar.save(ba);
		System.out.println("account saved");
	}

	
	
	
	@Override
	public void delete(int id) {
		
		
		
		bar.deleteById(id);
		System.out.println("deleted " + id);
		
	}

	

	@Override
	public List<BankAccountDTO> findAll() {
		List<BankAccountDTO> accounts = new ArrayList<BankAccountDTO>();
		List<BankAccount> acc = bar.findAll();
		
		for(BankAccount ba : acc) {
			BankAccountDTO b = new BankAccountDTO(ba);
//<<<<<<< HEAD
			System.out.println("BankAccountService.findAll()  "+ba.getUser());
			UserDTO  u = new UserDTO();
			u.setAddress(ba.getUser().getAddress());
//			u.setBankAccounts(ba.getUser().getBankAccounts());
			u.setFirstname(ba.getUser().getFirstname());
			u.setId(ba.getUser().getId());
			u.setIdentificationNumber(ba.getUser().getIdentificationNumber());
			u.setLastname(ba.getUser().getLastname());
//			u.setLegalEntity(b.getUser().getLastname());
			u.setPhoneNumber(ba.getUser().getPhoneNumber());
			u.setUsername(ba.getUser().getUsername());	
		
			System.err.println(ba.getBank().getName());
			
			BankDTO bankDto = new BankDTO();
			bankDto.setName(ba.getBank().getName());
			
			b.setBank(bankDto);
					
			b.setUser(u);
//=======
//			b.setUser(new UserDTO(ba.getUser()));
//			b.setBank(new BankDTO(ba.getBank()));
//>>>>>>> e8885f0b8342b86f62b50a2844bd818af3c870c8
			accounts.add(b);
			
		}
		return accounts;
	}

	
	
	@Override
	public BankAccountDTO findById(int id) {
	
		BankAccount bankAccount=	bar.findById(id).orElse(null);
		
		BankAccountDTO bankAccountDTO= new BankAccountDTO();
		bankAccountDTO.setBalance(bankAccount.getBalance());
				bankAccountDTO.setId(bankAccount.getId());
		bankAccountDTO.setNumber(bankAccount.getNumber());
		bankAccountDTO.setDate(bankAccount.getDate());
	
		
		return bankAccountDTO;
	}

	@Override
	public BankAccountDTO findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BankAccountDTO> findByUser(User user) {
		List<BankAccountDTO> accounts = new ArrayList<BankAccountDTO>();
		List<BankAccount> acc = bar.findByUser(user);
		
		for(BankAccount ba : acc) {
			BankAccountDTO b = new BankAccountDTO(ba);
			b.setUser(new UserDTO(user));
			b.setBank(new BankDTO(ba.getBank()));
			accounts.add(b);
		}
		return accounts;
	}


	

	@Override
	public BankAccount  DTOToBankAccount (BankAccountDTO transakcijeDTO) {
System.err.println("---DTOToBankAccount-----"+transakcijeDTO.getId());
		BankAccount bankAccount=new BankAccount();
		bankAccount.setId(transakcijeDTO.getId());
		bankAccount.setBalance(transakcijeDTO.getBalance());
//		bankAccount.setBank(transakcijeDTO.getBank);
//		bankAccount.setCurrency(transakcijeDTO.getCurrency());
//		bankAccount.setDailyConditions(transakcijeDTO.getDailyConditions());
		bankAccount.setDate(transakcijeDTO.getDate());
		bankAccount.setId(transakcijeDTO.getId());
		bankAccount.setNumber(transakcijeDTO.getNumber());
//		bankAccount.setUser(transakcijeDTO.getUser());
	
		return bankAccount;
	}



	@Override
	public List<BankAccountDTO> findByBank(BankDTO bank) {
		// TODO Auto-generated method stub
		return null;
	}





	@Override
	public List<BankAccountDTO> findByDate(Date date) {
		// TODO Auto-generated method stub
		return null;
	}





	@Override
	public BankAccount convert( String number1,int balance1,Bank  bank,User user,Currency currency) {
	
		
		BankAccount bankAccount=new BankAccount();
		 Date date = new Date();  
		bankAccount.setBalance(balance1);
		bankAccount.setDate(date);
		bankAccount.setCurrency(currency);
		bankAccount.setNumber(number1);
		bankAccount.setUser(user);
	
		bankAccount.setBank(bank);
		return bankAccount;
	}





	@Override
	public List<BankAccountDTO> findBankAccountForDelet(Integer id) {
		
		
		List<BankAccountDTO> accounts = new ArrayList<BankAccountDTO>();
		
		BankAccount bankAccount=	bar.findById(id).orElse(null);
		
		System.out.println("curency " +bankAccount.getCurrency());
		List<BankAccount> acc = bar.findBankAccountForDelet(bankAccount.getId(),bankAccount.getCurrency().getName());
		
		
		
		System.out.println("BankAccountService.findBankAccountForDelet()    -  "+acc.size());
		
		
		for(BankAccount ba : acc) {
			BankAccountDTO b = new BankAccountDTO(ba);
			System.out.println("BankAccountService.findAll()  "+ba.getUser());
			UserDTO  u = new UserDTO();
			u.setAddress(ba.getUser().getAddress());
			u.setFirstname(ba.getUser().getFirstname());
			u.setId(ba.getUser().getId());
			u.setIdentificationNumber(ba.getUser().getIdentificationNumber());
			u.setLastname(ba.getUser().getLastname());
			u.setPhoneNumber(ba.getUser().getPhoneNumber());
			u.setUsername(ba.getUser().getUsername());	
		
			
			BankDTO bankDto = new BankDTO();
			bankDto.setName(ba.getBank().getName());
			
			b.setBank(bankDto);
					
			b.setUser(u);
			accounts.add(b);
			
		}
		return accounts;
	}





	@Override
	public void delete(Integer idBankAccount, Integer idBankAccountForDelete) {
		
		
		BankAccount BankAccountForDelete=	bar.findById(idBankAccountForDelete).orElse(null);
		 
		
		BankAccount BankAccount=	bar.findById(idBankAccount).orElse(null);
		
		BankAccountClose bankAccountClose = new BankAccountClose();
	
	
		
		Date myDate = new Date();
		
		bankAccountClose.setDateOfClosure(myDate);
		bankAccountClose.setFrom(BankAccountForDelete);
		bankAccountClose.setTo(BankAccount);
	
		
		bankAccountCloseReposytory.save(bankAccountClose);

		double balance =BankAccount.getBalance()+BankAccountForDelete.getBalance();
		
		BankAccount.setBalance(balance);
		
		bar.save(BankAccount);
		
		bar.delete(BankAccountForDelete);
		
		
		
		
		
		
	}

	
	
	

	

}