package Narodna_Bank.demo.dto;

public class ModelDTO {
	private double amount;
	private String code;
	private String numberDebtor;
	private String numberRecipient; 
	private String purposeOfPayment;
	private int callNumber;
	private int model;
	private boolean murgently;
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getNumberDebtor() {
		return numberDebtor;
	}
	public void setNumberDebtor(String numberDebtor) {
		this.numberDebtor = numberDebtor;
	}
	public String getNumberRecipient() {
		return numberRecipient;
	}
	public void setNumberRecipient(String numberRecipient) {
		this.numberRecipient = numberRecipient;
	}
	public String getPurposeOfPayment() {
		return purposeOfPayment;
	}
	public void setPurposeOfPayment(String purposeOfPayment) {
		this.purposeOfPayment = purposeOfPayment;
	}
	public int getCallNumber() {
		return callNumber;
	}
	public void setCallNumber(int callNumber) {
		this.callNumber = callNumber;
	}
	public int getModel() {
		return model;
	}
	public void setModel(int model) {
		this.model = model;
	}
	public boolean isMurgently() {
		return murgently;
	}
	public void setMurgently(boolean murgently) {
		this.murgently = murgently;
	}
	public ModelDTO(double amount, String code, String numberDebtor, String numberRecipient, String purposeOfPayment,
			int callNumber, int model, boolean murgently) {
		super();
		this.amount = amount;
		this.code = code;
		this.numberDebtor = numberDebtor;
		this.numberRecipient = numberRecipient;
		this.purposeOfPayment = purposeOfPayment;
		this.callNumber = callNumber;
		this.model = model;
		this.murgently = murgently;
	}
	public ModelDTO() {
		super();
	}
	
	
}
