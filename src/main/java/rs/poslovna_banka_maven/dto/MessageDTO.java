package rs.poslovna_banka_maven.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rs.poslovna_banka_maven.entity.Bank;
import rs.poslovna_banka_maven.entity.Message;
import rs.poslovna_banka_maven.entity.Transakcije;
import rs.poslovna_banka_maven.entity.TypesOfMessages;

public class MessageDTO {
	private int id;
	private TypesOfMessages type;
	private double amount;
	private String codeOfValue;
	private Date dateOfValue;
	private Date date;
	public Bank debtorBank;
	
	public MessageDTO() {
	}
	
	public MessageDTO(Message m) {
		this.id = m.getId();
		this.type = m.getType();
		this.amount = m.getAmount();
		this.codeOfValue = m.getCodeOfValue();
		this.dateOfValue = m.getDateOfValue();
		this.date = m.getDate();
		this.debtorBank = m.getDebtorBank();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TypesOfMessages getType() {
		return type;
	}

	public void setType(TypesOfMessages type) {
		this.type = type;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCodeOfValue() {
		return codeOfValue;
	}

	public void setCodeOfValue(String codeOfValue) {
		this.codeOfValue = codeOfValue;
	}

	public Date getDateOfValue() {
		return dateOfValue;
	}

	public void setDateOfValue(Date dateOfValue) {
		this.dateOfValue = dateOfValue;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public Bank getDebtorBank() {
		return debtorBank;
	}

	public void setDebtorBank(Bank debtorBank) {
		this.debtorBank = debtorBank;
	}


}
