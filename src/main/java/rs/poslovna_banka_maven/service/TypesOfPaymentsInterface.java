package rs.poslovna_banka_maven.service;

import java.util.List;

import rs.poslovna_banka_maven.entity.TypesOfPayments;

public interface TypesOfPaymentsInterface  {
	List<TypesOfPayments> getAll();
	TypesOfPayments get(Integer id);
	TypesOfPayments insert(TypesOfPayments typesOfPayments);
	TypesOfPayments edit(TypesOfPayments typesOfPayments);
	void delete(Integer id);
}
