package rs.poslovna_banka_maven.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.poslovna_banka_maven.entity.Authority;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
	Authority findByName(String name);
}
