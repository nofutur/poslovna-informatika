package rs.poslovna_banka_maven.service;

import java.util.List;
import rs.poslovna_banka_maven.entity.CurrencyExchangeRate;

public interface CurrencyExchangeRateServiceInterface {

	public List<CurrencyExchangeRate> getAll();
	CurrencyExchangeRate get(Integer id);
	CurrencyExchangeRate insert(CurrencyExchangeRate currencyExchangeRate);
	CurrencyExchangeRate edit(CurrencyExchangeRate currencyExchangeRate);
	void delete(Integer id);
}
