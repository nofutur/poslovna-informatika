package rs.poslovna_banka_maven.service;

import java.util.List;
import java.util.Optional;

import rs.poslovna_banka_maven.entity.DailyCondition;



public interface DailyConditionServiceInterface {
	
	public List<DailyCondition>findAll();
	
	public DailyCondition findById(int id);
	
	
	
	public DailyCondition save(DailyCondition d);
	
	public void delete(DailyCondition d);

}