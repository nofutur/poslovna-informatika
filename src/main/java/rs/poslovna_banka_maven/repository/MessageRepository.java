package rs.poslovna_banka_maven.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rs.poslovna_banka_maven.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer>{

}
