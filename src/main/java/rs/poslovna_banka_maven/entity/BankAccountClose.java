package rs.poslovna_banka_maven.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="bank_accounts_close")
public class BankAccountClose {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="bank_account_close_id")
	private Integer id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_of_closure", unique=false, nullable=true)
	private Date dateOfClosure;
	
	@ManyToOne
	@JoinColumn(name = "bank_account_to_id", referencedColumnName = "bank_account_id", nullable = true)
	BankAccount to;
	
	@ManyToOne
	@JoinColumn(name="bank_account_from_id", referencedColumnName="bank_account_id", nullable=true)
	BankAccount from;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateOfClosure() {
		return dateOfClosure;
	}

	public void setDateOfClosure(Date dateOfClosure) {
		this.dateOfClosure = dateOfClosure;
	}

	public BankAccount getTo() {
		return to;
	}

	public void setTo(BankAccount to) {
		this.to = to;
	}

	public BankAccount getFrom() {
		return from;
	}

	public void setFrom(BankAccount from) {
		this.from = from;
	}
	
	

}
