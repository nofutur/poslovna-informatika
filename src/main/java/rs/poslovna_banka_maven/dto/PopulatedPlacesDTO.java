package rs.poslovna_banka_maven.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import rs.poslovna_banka_maven.entity.Country;
import rs.poslovna_banka_maven.entity.PopulatedPlace;
import rs.poslovna_banka_maven.entity.Transakcije;

public class PopulatedPlacesDTO {

    private int id;
	
	private String code;

	private String name;

	private int ptt;

	private Country country;

	public PopulatedPlacesDTO() {
		super();
	}

	public PopulatedPlacesDTO(PopulatedPlace p) {
		super();
		this.id = p.getId();
		this.code = p.getCode();
		this.name = p.getName();
		this.ptt = p.getPtt();
		this.country = p.getCountry();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPtt() {
		return ptt;
	}

	public void setPtt(int ptt) {
		this.ptt = ptt;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
}