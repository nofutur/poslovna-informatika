package rs.poslovna_banka_maven.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.poslovna_banka_maven.dto.BankDTO;

@Entity
@Table(name="Banks")
public class Bank {

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="bank_id")
	private int id;
	
	@Column(name = "bank_username", unique = true, nullable = false, length = 20)
	private String username;


	@Column(name = "bank_activated", unique = false, nullable = false)
	@ColumnDefault("1")
	private boolean activated;
	
	@Column(name="Code")
	private String code;
	
	@Column( nullable = false, name="Swift_code")
	private String swiftCode;
	
	@Column(name="Accounting_bill")
	private String accountingBill;
	
	@Column(name = "Tax_identification_number")
	private int TIN;
	
	@Column(name = "Name")
	private String name;
	
	@Column(name = "Address")
	private String address;
	
	@Column(name = "Web")
	private String web;
	
	@Column(name = "Phone")
	private String phone;
	
	@Column(name = "Fax")
	private String fax;
	
	@Column(name = "Balance")
	private double balance;
	
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "bank")
	public Set<BankAccount> bankAccounts = new HashSet<BankAccount>();
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "bank")
	public Set<ExchangeRateList> erl = new HashSet<ExchangeRateList>();
	

	public void addBankAccount(BankAccount bankAccount) {
		if (bankAccount.getBank() != null) {
			bankAccount.getBank().getBankAccounts().remove(bankAccount);
		}
		bankAccount.setBank(this);
		getBankAccounts().add(bankAccount);
	}
	
	public void removeBankAccount(BankAccount bankAccount) {
		bankAccount.setBank(null);
		getBankAccounts().remove(bankAccount);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSwiftCode() {
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public String getAccountingBill() {
		return accountingBill;
	}

	public void setAccountingBill(String accountingBill) {
		this.accountingBill = accountingBill;
	}

	public int getTIN() {
		return TIN;
	}

	public void setTIN(int TIN) {
		this.TIN = TIN;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getWeb() {
		return web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}
	
	public Set<BankAccount> getBankAccounts() {
		return bankAccounts;
	}

	public void setBankAccounts(Set<BankAccount> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

	public Set<ExchangeRateList> getErl() {
		return erl;
	}

	public void setErl(Set<ExchangeRateList> erl) {
		this.erl = erl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}



	public Bank() {}

	public Bank(int id, String username, boolean activated, String code, String swiftCode,
			String accountingBill, int tIN, String name, String address, String web, String phone, String fax,
			Set<BankAccount> bankAccounts, Set<ExchangeRateList> erl) {
		super();
		this.id = id;
		this.username = username;
		this.activated = activated;
		this.code = code;
		this.swiftCode = swiftCode;
		this.accountingBill = accountingBill;
		TIN = tIN;
		this.name = name;
		this.address = address;
		this.web = web;
		this.phone = phone;
		this.fax = fax;
		this.bankAccounts = bankAccounts;
		this.erl = erl;
	}

	public Bank(BankDTO bank) {
		this.id = bank.getId();
		this.username = bank.getUsername();
		this.activated = bank.isActivated();
		this.code = bank.getCode();
		this.swiftCode = bank.getSwiftCode();
		this.accountingBill = bank.getAccountingBill();
		this.TIN = bank.getTIN();
		this.name = bank.getName();
		this.address = bank.getAddress();
		this.web = bank.getWeb();
		this.phone = bank.getPhone();
		this.fax = bank.getFax();
		//this.bankAccounts = bankAccounts;
//		this.erl = bank.getErl();
	}
	
	
}
