package rs.poslovna_banka_maven.dto;

import java.util.Date;

import rs.poslovna_banka_maven.entity.TypesOfMessages;

public class MessageCreateDTO {
	
	private TypesOfMessages type;
	private double amount;
	private String codeOfValue;
	private Date dateOfValue;
	private Date date;
	public String debtorBank;
	public String creditorBank;
	
	
	public MessageCreateDTO(TypesOfMessages type, double amount, String codeOfValue, Date dateOfValue,
			Date date, String debtorBank, String creditorBank) {
		this.type = type;
		this.amount = amount;
		this.codeOfValue = codeOfValue;
		this.dateOfValue = dateOfValue;
		this.date = date;
		this.debtorBank = debtorBank;
		this.creditorBank = creditorBank;
	}


	public TypesOfMessages getType() {
		return type;
	}


	public void setType(TypesOfMessages type) {
		this.type = type;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public String getCodeOfValue() {
		return codeOfValue;
	}


	public void setCodeOfValue(String codeOfValue) {
		this.codeOfValue = codeOfValue;
	}


	public Date getDateOfValue() {
		return dateOfValue;
	}


	public void setDateOfValue(Date dateOfValue) {
		this.dateOfValue = dateOfValue;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getDebtorBank() {
		return debtorBank;
	}


	public void setDebtorBank(String debtorBank) {
		this.debtorBank = debtorBank;
	}


	public String getCreditorBank() {
		return creditorBank;
	}


	public void setCreditorBank(String creditorBank) {
		this.creditorBank = creditorBank;
	}
}

