package rs.poslovna_banka_maven.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.Principal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Convert;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import rs.poslovna_banka_maven.dto.BankAccountDTO;
import rs.poslovna_banka_maven.dto.BankDTO;
import rs.poslovna_banka_maven.dto.UserDTO;
import rs.poslovna_banka_maven.entity.Bank;
import rs.poslovna_banka_maven.entity.User;
import rs.poslovna_banka_maven.repository.BankRepository;
import rs.poslovna_banka_maven.repository.CurrencyRepository;
import rs.poslovna_banka_maven.repository.UserRepository;
import rs.poslovna_banka_maven.entity.BankAccount;
import rs.poslovna_banka_maven.entity.Currency;
import rs.poslovna_banka_maven.service.BankAccountServiceInterface;
import rs.poslovna_banka_maven.service.BankServiceInterface;
import rs.poslovna_banka_maven.service.UserServiceInterface;


@RestController
@RequestMapping("/api/bankAccount/")
public class BankAccountController {
	
	
	@Autowired
	BankAccountServiceInterface bankAccountService;
	
	@Autowired
	BankServiceInterface bankService;
	@Autowired
	UserRepository userRepository;
	@Autowired
	CurrencyRepository currencyRepository;
	
	@Autowired
	BankRepository bankRepository;
	
	@Autowired 
	UserServiceInterface usi;
	
	
    private static final String DEFAULT_FILE_NAME = "Transakcije.xml";
 
    @Autowired
    private ServletContext servletContext;
    
	@GetMapping()
    public ResponseEntity<List<BankAccountDTO>> getAll(Principal principal) {
		System.err.println("-BankAccountServiceInterface-");
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentUserName = "";
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
		    currentUserName = authentication.getName();
		    System.out.println(currentUserName);
		}		
		UserDTO user = usi.findByUsername(currentUserName);
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		List<BankAccountDTO> bankAccountsDTO = bankAccountService.findAll();
//        List<BankAccountDTO> bankAccountsDTO = bankAccountService.findByUser(user);
	    System.out.println(bankAccountsDTO.toString());

        return new ResponseEntity<>(bankAccountsDTO,HttpStatus.OK);
    }
	

	
	
	
	@PostMapping(path = "{number1}/{balance1}/{user1}/{codeOfValue1}/{bank1}", consumes = "application/json")
	public ResponseEntity<BankAccountDTO> add(
			
			@PathVariable String number1,
			@PathVariable int balance1,
			@PathVariable String codeOfValue1,
			
			
			@PathVariable String  bank1,
			@PathVariable Integer user1,
			Principal principal){
	
		
		
		BankAccountDTO bankAccountDTO = new BankAccountDTO();
		bankAccountDTO.setBalance(balance1);
		bankAccountDTO.setNumber(number1);
System.out.println("BankAccountController.add()");
System.err.println("dddddddddd  "+user1);
		User user = userRepository.findById(user1).orElse(null);
		System.err.println("dddddddddd  "+user.getUsername());

		
		Bank bank =bankRepository.findByUsername(bank1);
		Currency cur = currencyRepository.findById(1).orElse(null);
		
		
		bankAccountService.save(bankAccountService.convert(number1,balance1,bank,user,cur));

		
		return new ResponseEntity<>(bankAccountDTO, HttpStatus.OK);
		
	}
		
	
	@DeleteMapping(path = "{id}")
	public ResponseEntity<List<BankAccountDTO>> switchingAccounts(@PathVariable Integer id) {

		System.out.println("delete account " + id);
		
		if (id == null)
			{		
				return new ResponseEntity<List<BankAccountDTO>>(HttpStatus.BAD_REQUEST);
			}

		
		
		List<BankAccountDTO>bankAccountDTOs = bankAccountService.findBankAccountForDelet(id);
	
		
		 return new ResponseEntity<>(bankAccountDTOs,HttpStatus.OK);
		
		
		
	
	
	}
	
	
	@DeleteMapping(path = "{idBankAccount}/{idBankAccountForDelete}")
	public ResponseEntity<List<BankAccountDTO>> deleteBankAccount(@PathVariable Integer idBankAccount,@PathVariable Integer idBankAccountForDelete) {

		System.out.println("delete account " + idBankAccount);
		System.out.println("delete account " + idBankAccountForDelete);

		
		if (idBankAccountForDelete == null)
			{		
			System.err.println(" 1 HttpStatus.BAD_REQUEST...");

				return new ResponseEntity<List<BankAccountDTO>>(HttpStatus.BAD_REQUEST);
			}
	
		if (idBankAccount == null)
		{		
			System.err.println(" 2 HttpStatus.BAD_REQUEST...");
			return new ResponseEntity<List<BankAccountDTO>>(HttpStatus.BAD_REQUEST);
		}
		
		BankAccountDTO BankAccountForDelete = bankAccountService.findById(idBankAccountForDelete);
	
		if (BankAccountForDelete == null)
		{		
			System.err.println(" 3 HttpStatus.NOT_FOUND...");

			return new ResponseEntity<List<BankAccountDTO>>(HttpStatus.NOT_FOUND);
		}
		
		
		
		BankAccountDTO BankAccount = bankAccountService.findById(idBankAccount);
		if (BankAccount == null)
		{		
			System.err.println(" 4 HttpStatus.NOT_FOUND...");
			return new ResponseEntity<List<BankAccountDTO>>(HttpStatus.NOT_FOUND);
		}
		
		
		
		 bankAccountService.delete(idBankAccount, idBankAccountForDelete);
	
		
		 return new ResponseEntity<>(HttpStatus.OK);

	}

	@RequestMapping(value = "/reportIndividualPDF" , method = RequestMethod.GET)
	public ResponseEntity<?> reportToPDF(Principal principal) {
		System.err.println("-------reportIndividualPDF-----------");
		
		UserDTO user = usi.findByUsername(principal.getName());
		List<BankAccountDTO> bankAccounts = bankAccountService.findByUser(user);
		for (int i = 0; i < bankAccounts.size(); i++) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("number", bankAccounts.get(i).getNumber());
		
		try {
			
			Connection con=(Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/poslovna_banka?useSSL=true&createDatabaseIfNotExist=true","root","root");
			JasperReport jasperReport = JasperCompileManager.compileReport(getClass().getResource("/jasper/stanjeRacuna.jrxml").openStream());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);			
			
			JasperExportManager.exportReportToPdfFile(jasperPrint, "C:\\lucine\\BankXml\\Racuni fizickih lica.pdf");
			//promenite putanju za probu.
	
		
			
			
			 MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "Racuni fizickih lica.pdf");
		        System.out.println("fileName: " + "Racuni fizickih lica.pdf");
		        System.out.println("mediaType: " + mediaType);
		 
		        File file = new File("C:\\\\lucine\\\\BankXml\\\\Racuni fizickih lica.pdf");
		        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		        System.out.println("mediaType: ");
		        return ResponseEntity.ok()
		        		
		    	    

		                // Content-Disposition
		                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
		                // Content-Type
		                .contentType(mediaType)
		                // Contet-Length
		                .contentLength(file.length()) //
		                .body(resource);
		
		
		}catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	
		return new ResponseEntity<>(HttpStatus.OK);
		}
	

	
	    
	    
	    
	@GetMapping("/reportLegalPDF")
	public ResponseEntity<InputStreamResource> reportLegalToPDF(Principal principal) throws FileNotFoundException {
		System.err.println("-------reportIndividualPDF-----------");

		UserDTO user = usi.findByUsername(principal.getName());
		List<BankAccountDTO> bankAccounts = bankAccountService.findByUser(user);
		for (int i = 0; i < bankAccounts.size(); i++) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			
			parameters.put("number", bankAccounts.get(i).getNumber());
		try {
			Connection con=(Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/poslovna_banka?useSSL=true&createDatabaseIfNotExist=true","root","root");
			JasperReport jasperReport = JasperCompileManager.compileReport(getClass().getResource("/jasper/StanjeRacunaPravnaLica.jrxml").openStream());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);
			
			//promenite putanju
			JasperExportManager.exportReportToPdfFile(jasperPrint, "C:\\lucine\\BankXml\\Racuni pravna lica.pdf");
		
		
			
			
		
			   MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "Racuni pravna lica.pdf");
		        System.out.println("fileName: " + "Racuni pravna lica.pdf");
		        System.out.println("mediaType: " + mediaType);
		 
		        File file = new File("C:\\\\lucine\\\\BankXml\\\\Racuni pravna lica.pdf");
		        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
		        System.out.println("mediaType: ");
		        return ResponseEntity.ok()
		        		
		    	    

		                // Content-Disposition
		                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
		                // Content-Type
		                .contentType(mediaType)
		                // Contet-Length
		                .contentLength(file.length()) //
		                .body(resource);
		
		
		
		
		}catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	return null;
	     
	}
	
}
