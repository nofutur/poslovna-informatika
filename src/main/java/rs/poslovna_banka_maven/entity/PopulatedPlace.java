package rs.poslovna_banka_maven.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.poslovna_banka_maven.dto.PopulatedPlacesDTO;

@Entity
@Table(name = "populated_place")
public class PopulatedPlace implements Serializable{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "populated_place_id", unique = true, nullable = false)
	private int id;
	
	@Column(name = "code", nullable = false)
	private String code;
	
	@Column(name = "name",  nullable = false)
	private String name;
	
	@Column(name = "ptt", unique = true, nullable = false)
	private int ptt;
	
	@ManyToOne
	@JoinColumn(name="id", referencedColumnName="id", nullable=false)
	private Country country;

	@OneToMany(cascade= {ALL}, fetch=LAZY, mappedBy="place")
	@JsonIgnore
	private Set<Transakcije> transactions;

	public PopulatedPlace() {
		super();
	}

	public PopulatedPlace(PopulatedPlacesDTO p) {
		super();
		this.id = p.getId();
		this.code = p.getCode();
		this.name = p.getName();
		this.ptt = p.getPtt();
		this.country = p.getCountry();
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPtt() {
		return ptt;
	}

	public void setPtt(int ptt) {
		this.ptt = ptt;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Set<Transakcije> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<Transakcije> transactions) {
		this.transactions = transactions;
	}
}
