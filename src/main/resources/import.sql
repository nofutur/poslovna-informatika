INSERT INTO AUTHORITY(name) VALUES ('ROLE_USER');
INSERT INTO AUTHORITY(name) VALUES ('ROLE_ADMIN');

INSERT INTO users(user_activated, user_address, user_firstname, user_id_number, user_lastname, user_legal_entity, user_phone_number, user_username, user_password) VALUES (true, 'Jevrejska 10', 'Aleksa', '2305980678471', 'Aleksic', false, '0603333333', 'aaleksic', '$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi');
INSERT INTO users(user_activated, user_address, user_firstname, user_id_number, user_lastname, user_legal_entity, user_phone_number, user_username, user_password) VALUES (true, 'Tolstojeva 56', 'Auto skola', '685749852', 'Tocak', true, '0216874874', 'tocak', '$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi');
INSERT INTO users(user_activated, user_address, user_firstname, user_id_number, user_lastname, user_legal_entity, user_phone_number, user_username, user_password) VALUES (true, 'Dunavska 5', 'Milica', '2305986674857', 'Milanovic', false, '0644444444', 'mmilanovic', '$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi');
INSERT INTO users(user_activated, user_address, user_firstname, user_id_number, user_lastname, user_legal_entity, user_phone_number, user_username, user_password) VALUES (true, 'Cara Dusana 62', 'Cvecara', '697485741', 'Neven', true, '0214444444', 'neven', '$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi');

INSERT INTO banks(tax_identification_number, accounting_bill, address, code, fax, name, phone, swift_code, web, bank_username,balance) VALUES (241574896, '908-20001-18', 'Bulevar oslobođenja 13', '200', 'pbanka.fax', 'Banka Postanska stedionica', '0214578476', 'SBPORSBG', 'pbanka.com','pbanka',12300000);


INSERT INTO currencies(code_of_currency, name) VALUES ('EUR', 'Evro');
INSERT INTO currencies(code_of_currency, name) VALUES ('BAM', 'BiH konvertibilna marka');
INSERT INTO currencies(code_of_currency, name) VALUES ('HRK', 'Hrvatska kuna');
INSERT INTO currencies(code_of_currency, name) VALUES ('CHF', 'Svajcarski franak');

INSERT INTO bank_accounts(balance, date_open, number, valid, bank_id, currency_id, user_id) VALUES(500.0, '2019-05-04', '100-1023102101021-11', true, 1, 1, 1);
INSERT INTO bank_accounts(balance, date_open, number, valid, bank_id, currency_id, user_id) VALUES(400.0, '2018-04-10', '120-2100000212300-20', true, 1, 2, 2);
INSERT INTO bank_accounts(balance, date_open, number, valid, bank_id, currency_id, user_id) VALUES(1000.0, '2017-03-02', '110-1100001300000-10', true, 1, 2, 3);
INSERT INTO bank_accounts(balance, date_open, number, valid, bank_id, currency_id, user_id) VALUES(2000.0, '2018-05-01', '110-1200007800000-10', true, 1, 2, 4);

INSERT INTO country(code, name, currency_id) VALUES ('DEU', 'Nemacka', 1);
INSERT INTO country(code, name, currency_id) VALUES ('BIH', 'Bosna i Hercegovina', 2);
INSERT INTO country(code, name, currency_id) VALUES ('HRV', 'Hrvatska', 3);
INSERT INTO country(code, name, currency_id) VALUES ('CHE', 'Svajcarska', 4);

INSERT INTO exchange_rate_list(applied_by, Date, number_of_exchange_rate_list, bank_id_erl) VALUES ('2018-05-10', '2018-05-11', 1, 1);


INSERT INTO currency_exchange_rate(buying, middle, ordinal_number, selling, currency_id, exchange_rate_list_id) VALUES ('118.20', '118.90', 1, '119.0', 1, 1);
INSERT INTO currency_exchange_rate(buying, middle, ordinal_number, selling, currency_id, exchange_rate_list_id) VALUES ('56.20', '57.90', 2, '57.0', 2, 1);
INSERT INTO currency_exchange_rate(buying, middle, ordinal_number, selling, currency_id, exchange_rate_list_id) VALUES ('15.20', '16.90', 3, '16.0', 3, 1);
INSERT INTO currency_exchange_rate(buying, middle, ordinal_number, selling, currency_id, exchange_rate_list_id) VALUES ('112.80', '112.20', 4, '113.0', 4, 1);

INSERT INTO daily_Conditions(new_state, previous_state, bank_account_id) VALUES(200.0, 300.0, 1);
INSERT INTO daily_Conditions(new_state, previous_state, bank_account_id) VALUES(0, 400.0, 2);
INSERT INTO daily_Conditions(new_state, previous_state, bank_account_id) VALUES(250.0, 1000.0, 3);

INSERT INTO messages(amount, code_of_value, message_date, date_of_value, message_type, bank_id_debtor) VALUES(200.0, 'EUR', '2019-01-20', '2019-02-20', 'MT102', 1);
INSERT INTO messages(amount, code_of_value, message_date, date_of_value, message_type, bank_id_debtor) VALUES(2000.0, 'BIH', '2020-02-20', '2020-03-20', 'MT103', 1);

INSERT INTO populated_place(code, name, ptt, id) VALUES ('B', 'Berlin', 10115, 1);
INSERT INTO populated_place(code, name, ptt, id) VALUES ('SA', 'Sarajevo', 71000, 2);

INSERT INTO types_of_payments(name, payment_type_code) VALUES ('Gotovina', 'G');
INSERT INTO types_of_payments(name, payment_type_code) VALUES ('Kartica', 'K');

INSERT INTO transactions(amount, call_number, creditor, date_of_receipt, date_of_value, debit_model, debtor_account,error, purpose_of_payment, status, urgently, daily_condition_id,  bank_account_id_debtor, message_id, type_of_payment_id, populated_place_id,currencies_currency_id) VALUES (200, 11, 'aaleksic', '2019-02-06', '2019-02-06', 97,100-1023102101021-10,  null,'Uplata pazara' ,true, false, 1,1, 1, 1, 1,null );
INSERT INTO transactions(amount, call_number, creditor, date_of_receipt, date_of_value, debit_model, debtor_account,error, purpose_of_payment, status, urgently, daily_condition_id,  bank_account_id_debtor, message_id, type_of_payment_id, populated_place_id,currencies_currency_id) VALUES (100, 11, 'aaleksic', '2019-02-10', '2019-02-06', 97,100-1023102101021-10,  null,'Uplata pazara' ,true, false, 1,1, 1, 1, 1,null );
--INSERT INTO transactions(amount, call_number, creditor, date_of_receipt, date_of_value, debit_model, debtor_account,error, purpose_of_payment, status, urgently, daily_condition_id,  bank_account_id_debtor, message_id, type_of_payment_id, populated_place_id,currencies_currency_id) VALUES (205, 11, 'neven', '2019-02-06', '2019-02-06', 97,100-1023102101021-10,  null,'Uplata pazara' ,true, false, 1,2, 1, 1, 1,null );
--INSERT INTO transactions(amount, call_number, creditor, date_of_receipt, date_of_value, debit_model, debtor_account,error, purpose_of_payment, status, urgently, daily_condition_id,  bank_account_id_debtor, message_id, type_of_payment_id, populated_place_id,currencies_currency_id) VALUES (400, 11, 'tocak', '2019-02-06', '2019-02-06', 97,100-1023102101021-10,  null,'Uplata pazara' ,true, false, 1,3, 1, 1, 1,null );


INSERT INTO USER_AUTHORITY (user_id, AUTHORITY_NAME) VALUES (1, 'ROLE_USER');
INSERT INTO USER_AUTHORITY (user_id, AUTHORITY_NAME) VALUES (2, 'ROLE_ADMIN');
INSERT INTO USER_AUTHORITY (user_id, AUTHORITY_NAME) VALUES (3, 'ROLE_USER');
INSERT INTO USER_AUTHORITY (user_id, AUTHORITY_NAME) VALUES (4, 'ROLE_ADMIN');

