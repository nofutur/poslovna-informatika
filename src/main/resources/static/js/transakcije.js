var selectRacunDuznika;
var selectRacunPoverioca;
var racun;

var amount;
var code;
var numberDebtor;
var numberRecipient;
var purposeOfPayment;
var callNumber;
var model;
var urgently;




$(document).ready(function () {
	
if(getJwtToken()==null){
    window.location.replace('login.html');
}


	amount= $("#interbankTransferForm [name='amount']");
	code=$("#interbankTransferForm [name='codeOfValue']");
	numberDebtor=$("#interbankTransferForm [name='numberDebtor']");
	numberRecipient=$('#selectRacunDuznika');
	purposeOfPayment=$("#interbankTransferForm [name='svrha']");
	callNumber=$("#interbankTransferForm [name='callOnNumber']");
	model=$("#interbankTransferForm [name='type']");
	urgently=document.getElementById("urgentlyOfTransaction");
	
    $("#izvestajFizickaLica").click(function(e){
    	console.log("izvestajFizickaLica")

    	$.ajax({
    	    url: "http://localhost:8080/api/bankAccount/reportIndividualPDF",
    	    headers: createAuthorizationTokenHeader(),
    	    method: 'GET',
    	 
    	    
    	    xhrFields: {
                responseType: 'blob'
            },
    	    success: function (data) {
    	    	console.log(data)
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(data);
                a.href = url;
                a.download = 'Racuni fizickih lica.pdf';
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
            }
    	    
    	    
    	    
    	  });
    	});
    
    
    $("#izvestaj").click(function(e){
    	console.log("izvestaj")

    	$.ajax({
    	    url: "http://localhost:8080/api/bankAccount/reportLegalPDF",
    	    headers: createAuthorizationTokenHeader(),
    	    method: 'GET',
    	
            xhrFields: {
                responseType: 'blob'
            },
    	    success: function (data) {
    	    	console.log(data)
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(data);
                a.href = url;
                a.download = 'Racuni pravna lica.pdf';
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
            }
    	
    
    });
    });


//    $("#izvestaj").click(function(e){
//    	$.ajax({
//    	    url: "http://localhost:8080/api/bankAccount/reportLegalPDF",
//    	    headers: createAuthorizationTokenHeader(),
//    	    method: 'GET',
//    	    
//    	    success: function (data) {
//                //Convert the Byte Data to BLOB object.
//                var blob = new Blob([data], { type: "application/octetstream" });
//
//                //Check the Browser type and download the File.
//                var isIE = false || !!document.documentMode;
//                if (isIE) {
//                    window.navigator.msSaveBlob(blob, "Racuni fizickih lica.pdf");
//                } else {
//                    var url = window.URL || window.webkitURL;
//                    link = url.createObjectURL(blob);
//                    var a = $("<a />");
//                    a.attr("download", "Racuni fizickih lica.pdf");
//                    a.attr("href", link);
//                    $("body").append(a);
//                    a[0].click();
//                    $("body").remove(a);
//                }
//            }
//    	
//    	  });
//    });
	
	selectRacunDuznika = $('#selectRacunDuznika');
//	fetchBankAccountsForUser();

	getTransaction();
	fetchBankAccountsForUser();

	
	function getTransaction(){
	
		
		console.log( createAuthorizationTokenHeader())
	$.ajax({
		url: "http://localhost:8080/api/transactions/2001-08-17/2081-08-17",
		type: "GET",
		
		headers:  createAuthorizationTokenHeader(),

	}).then(
		function (data) {
			console.log('jaa')
			console.log(data);
			$("#mPrenosTable").empty();
			 for (i = 0; i < data.length; i++) {
				 console.log(data[i].debtorAccount)
			 	newRow =
			 		"<tr>"
			 		+ "<td class=\"debtorBank\">" + data[i].creditor + "</td>"
			 		+ "<td class=\"amount\">" + data[i].debtor.number + "</td>"
			 		+ "<td class=\"codeOfValue\">" + data[i].amount + "</td>"
			 		+ "<td class=\"dateOfValue\">" + data[i].debtorAccount + "</td>"
			 		+ "<td class=\"type\">" + data[i].purposeOfPayment + "</td>"
			 		+ "<td class=\"date\">" + data[i].urgently + "</td>"
			 		+ "<td class=\"date\">" + data[i].dateOfReceipt.substring(0,10) + "</td>"
			 		+ "<td class=\"type\">" + data[i].callNumber + "</td>"
			 		+ "<td class=\"type\">" + data[i].debitModel + "</td>"
			 		
//					+" <td> <button  onclick='TransactionDelete("+data[i].id+");' >DELETE </button> </td>"
		
			 		+ "</tr>"
			 	$("#mPrenosTable").append(newRow)
			 }
		});
	}
	
	   $("#vremePocetno").change(function(){
		   getTransaction1();

	    });
	   $("#vremeKrajnje").change(function(){
		   getTransaction1();

	    });
	   
	   $("#brojDuznikaText").change(function(){
		   findByDebitor();
//			
	    });
	   
	   $("#brojPrimaocaText").change(function(){
		   findByRecipient();
//			
	    });
	   
	   function findByRecipient() {
			
		   var brojPrimaocaText = document.getElementById("brojPrimaocaText").value;
		
			console.log("brojDuznikaText"+brojPrimaocaText)
			
				$.ajax({
					
					url: "http://localhost:8080/api/transactions/findByRecipient/" + brojPrimaocaText,
					type: "GET",
					headers:  createAuthorizationTokenHeader(),
				
					success: function (data) {
						console.log('oks123'+data)
						console.log(data);
						$("#mPrenosTable").empty();
						 for (i = 0; i < data.length; i++) {
							 	newRow =
							 		"<tr>"
							 		+ "<td class=\"debtorBank\">" + data[i].creditor + "</td>"
							 		+ "<td class=\"amount\">" + data[i].debtor.number + "</td>"
							 		+ "<td class=\"codeOfValue\">" + data[i].amount + "</td>"
							 		+ "<td class=\"dateOfValue\">" + data[i].debtorAccount + "</td>"
							 		+ "<td class=\"type\">" + data[i].purposeOfPayment + "</td>"
							 		+ "<td class=\"date\">" + data[i].urgently + "</td>"
							 		+ "<td class=\"date\">" + data[i].dateOfReceipt.substring(0,10) + "</td>"
							 		+ "<td class=\"type\">" + data[i].callNumber + "</td>"
							 		+ "<td class=\"type\">" + data[i].debitModel + "</td>"
							 		+ "</tr>"
							 	$("#mPrenosTable").append(newRow)
							 }

					}
				});
		}
	   
	   function findByDebitor() {
			
		   var brojDuznikaText = document.getElementById("brojDuznikaText").value;
		
			console.log("brojDuznikaText"+brojDuznikaText)
			
				$.ajax({
					
					url: "http://localhost:8080/api/transactions/" + brojDuznikaText,
					type: "GET",
					headers:  createAuthorizationTokenHeader(),
				
					success: function (data) {
						console.log('oks123'+data)
						console.log(data);
						$("#mPrenosTable").empty();
						 for (i = 0; i < data.length; i++) {
							 	newRow =
							 		"<tr>"
							 		+ "<td class=\"debtorBank\">" + data[i].creditor + "</td>"
							 		+ "<td class=\"amount\">" + data[i].debtor.number + "</td>"
							 		+ "<td class=\"codeOfValue\">" + data[i].amount + "</td>"
							 		+ "<td class=\"dateOfValue\">" + data[i].debtorAccount + "</td>"
							 		+ "<td class=\"type\">" + data[i].purposeOfPayment + "</td>"
							 		+ "<td class=\"date\">" + data[i].urgently + "</td>"
							 		+ "<td class=\"date\">" + data[i].dateOfReceipt.substring(0,10) + "</td>"
							 		+ "<td class=\"type\">" + data[i].callNumber + "</td>"
							 		+ "<td class=\"type\">" + data[i].debitModel + "</td>"
							 		+ "</tr>"
							 	$("#mPrenosTable").append(newRow)
							 }

					}
				});
		}
	   
	
	function getTransaction1() {
		
		var date1 = document.getElementById("vremePocetno").value;
		var date2 = document.getElementById("vremeKrajnje").value;
	
		console.log("brojDuznikaText"+brojDuznikaText)
		console.log(date1, date2);
			$.ajax({
				
				url: "http://localhost:8080/api/transactions/" + date1 + "/" + date2 ,
				type: "GET",
				headers:  createAuthorizationTokenHeader(),
			
				success: function (data) {
					console.log('oks123'+data)
					console.log(data);
					$("#mPrenosTable").empty();
					 for (i = 0; i < data.length; i++) {
						 	newRow =
						 		"<tr>"
						 		+ "<td class=\"debtorBank\">" + data[i].creditor + "</td>"
						 		+ "<td class=\"amount\">" + data[i].debtor.number + "</td>"
						 		+ "<td class=\"codeOfValue\">" + data[i].amount + "</td>"
						 		+ "<td class=\"dateOfValue\">" + data[i].debtorAccount + "</td>"
						 		+ "<td class=\"type\">" + data[i].purposeOfPayment + "</td>"
						 		+ "<td class=\"date\">" + data[i].urgently + "</td>"
						 		+ "<td class=\"date\">" + data[i].dateOfReceipt.substring(0,10) + "</td>"
						 		+ "<td class=\"type\">" + data[i].callNumber + "</td>"
						 		+ "<td class=\"type\">" + data[i].debitModel + "</td>"
						 		+ "</tr>"
						 	$("#mPrenosTable").append(newRow)
						 }

				}
			});
	}
	
	

	
	  
	

	
	
	   $("#addITransfer").click(function(){
		   if (amount.val() === '') {
			   alert("polje iznos je obavezno");
			   return false;
		   }
		   
		   if (code.val() === '') {
			   alert("Racun iznos je obavezno");
			   return false;
		   }
		   
//		   if (numberDebtor.val() === '') {
//			   alert("Poverioca iznos je obavezno");
//			   return false;
//		   }
		   
		   
		   if (model.val() === '') {
			   alert("polje model je obavezno");
			   return false;
		   }
		   
		 
		   
		   console.log(" ---123----------")
		   var formData = {
				amount: amount.val(),
				code: code.val(),
				numberDebtor: numberDebtor.val(),
				numberRecipient: numberRecipient.val(),
				purposeOfPayment: purposeOfPayment.val(),
				callNumber: callNumber.val(),
				model: model.val(), 
				urgently:  urgently.checked
			
		   };
	    	
		
	    	  formData = JSON.stringify({
	    		  	numberRecipient:formData.numberRecipient,
	    		  	amount:formData.amount,
	    		  	code: formData.code,
	    		  	numberDebtor: formData.numberDebtor,
	    		  	purposeOfPayment:formData.purposeOfPayment,
	    		  	callNumber: formData.callNumber,
	    		  	model: formData.model,
	    		  	urgently: formData.urgently
              });
	    	  
	    	  
              $.ajax({
                  url: "http://localhost:8080/api/transactions/",
                  type: "POST",
                  data: formData,
                  contentType: "application/json",
                  headers:  createAuthorizationTokenHeader(),

                  datatype: 'json',
                  success: function () {
               	   alert('uspesno dodata transakcija!')

                      getRacuni();
                  },
                  error: function () {
                      alert('Wrong!')
                  }
              });


	    });
	});


function TransactionDelete(id) {
	 if(confirm('da li zelite da obrisete transakciju?')
){
    $.ajax({
        url: "http://localhost:8080/api/transactions/"+id,
        type: "DELETE",
        datatype: 'json',
        success: function () {
        	   alert('uspesno obrisana transakcija!')
        },
        error: function () {
            alert('greska!')
        }
    });
    
	 }
	
	
}

//function fetchBankAccountsForUser() {
//	$.ajax({
//		type: "GET",
//		url: "api/bankAccount",
//		dataType: "json",
//		contentType: "application/json; charset=utf-8",
//		headers: createAuthorizationTokenHeader(),
//		success: function (data) {
//			console.log('oks')
//			racun=data[0].number;
//			console.log(data);
//			console.log('joks')
//			$.each(data, function (i, item) {
//				$(selectRacunDuznika).append($('<option>', {
//					value: item.number,
//					text: item.number
//				}));
//			});
//		}
//	});
//};


function izvodTransakcije() {
	console.log("izvodTransakcije")
	var vremePocetno = document.getElementById("vremePocetno").value;
	var vremeKrajnje = document.getElementById("vremeKrajnje").value;
	
	

	if(vremePocetno==null){
		vremePocetno="";
	}
	if(vremeKrajnje==null){
		vremeKrajnje="";
	}
	$.ajax({	
		url: "http://localhost:8080/api/transactions/izvodPDF/" + vremePocetno + "/" + vremeKrajnje,
		  headers:  createAuthorizationTokenHeader(),		method: 'GET',

		   xhrFields: {
               responseType: 'blob'
           },
   	    success: function (data) {
   	    	console.log(data)
               var a = document.createElement('a');
               var url = window.URL.createObjectURL(data);
               a.href = url;
               a.download = 'Izvod klienta.pdf';
               document.body.append(a);
               a.click();
               a.remove();
               window.URL.revokeObjectURL(url);
           }
		
		
		
		
		});
}


function searchTransaction() {
	var date1 = document.getElementById("vremePocetno").value;
	var date2 = document.getElementById("vremeKrajnje").value;
		
	
	
	
	
	
	
	
	console.log("searchTransaction"),
	
	
	$.ajax({
		url: "http://localhost:8080/api/transactions/searchTransaction/" + date1 + "/" + date2,
	    headers: createAuthorizationTokenHeader(),
	    method: 'GET',
	 
	    
	    xhrFields: {
            responseType: 'blob'
        },
	    success: function (data) {
	    	console.log(data)
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = 'Transakcije.xml';
            document.body.append(a);
            a.click();
            a.remove();
            window.URL.revokeObjectURL(url);
        }
	    
	    
	    
	  });
	
	
	
	
	
	
	

	
}

//function izvodTransakcije(){
//	console.log("izvodTransakcije")
//	var date1 = document.getElementById("vremePocetno").value;
//	var date2 = document.getElementById("vremeKrajnje").value;
//	$.ajax({	
//		url: "http://localhost:8080/api/transactions/izvodPDF/" + date1 + "/" + date2,
//		headers:  createAuthorizationTokenHeader(),
//
//
//		method: 'GET',
//
//		
//		
//		
//		   xhrFields: {
//               responseType: 'blob'
//           },
//   	    success: function (data) {
//   	    	console.log(data)
//   	     var a = document.createElement('a');
//            var url = window.URL.createObjectURL(data);
//            a.href = url;
//            a.download = 'Racuni fizickih lica.pdf';
//            document.body.append(a);
//            a.click();
//            a.remove();
//            window.URL.revokeObjectURL(url);
//           }
//		
//		
//		
//		
//		
//		
//		
//		});
//	};

	
	function fetchBankAccountsForUser() {
		console.log("fetchBankAccountsForUser")
		$.ajax({
			type: "GET",
			url: "http://localhost:8080/api/bankAccount/",
			dataType: "json",
			contentType: "application/json; charset=utf-8",
			headers:  createAuthorizationTokenHeader(),

			success: function (data) {
				racun=data[0].number;
				console.log(data);
				$.each(data, function (i, item) {
					$(selectRacunDuznika).append($('<option>', {
						value: item.number,
						text: item.number
					}));
				});
			}
		});
	};


//
//$("#search").click(function () {
//
//	var date1 = document.getElementById("vremePocetno").value;
//	var date2 = document.getElementById("vremeKrajnje").value;
//	console.log(date1, date2);
//		$.ajax({
//			
//			url: "http://localhost:8080/api/transactions/searchTransaction/" + vremePocetno + "/" + vremeKrajnje,
//			type: "GET",
//			Authorization: createAuthorizationTokenHeader().Authorization
//			success: function (data) {
//				console.log('oks123')
//				console.log(data);
//				$("#mPrenosTable").empty();
//				 for (i = 0; i < data.length; i++) {
//					 	newRow =
//					 		"<tr>"
//					 		+ "<td class=\"debtorBank\">" + data[i].creditor + "</td>"
//					 		+ "<td class=\"amount\">" + data[i].debtor.number + "</td>"
//					 		+ "<td class=\"codeOfValue\">" + data[i].amount + "</td>"
//					 		+ "<td class=\"dateOfValue\">" + data[i].debtorAccount + "</td>"
//					 		+ "<td class=\"type\">" + data[i].purposeOfPayment + "</td>"
//					 		+ "<td class=\"date\">" + data[i].urgently + "</td>"
//					 		+ "<td class=\"date\">" + data[i].dateOfReceipt.substring(0,10) + "</td>"
//					 		+ "<td class=\"type\">" + data[i].callNumber + "</td>"
//					 		+ "<td class=\"type\">" + data[i].debitModel + "</td>"
//					 		+ "</tr>"
//					 	$("#mPrenosTable").append(newRow)
//					 }
//			}
//		});
//	});
