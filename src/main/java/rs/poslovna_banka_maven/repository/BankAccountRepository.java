package rs.poslovna_banka_maven.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import rs.poslovna_banka_maven.entity.Bank;
import rs.poslovna_banka_maven.entity.BankAccount;
import rs.poslovna_banka_maven.entity.Transakcije;
import rs.poslovna_banka_maven.entity.User;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Integer>{

	List<BankAccount> findByUser(User user);
	List<BankAccount> findByBank(Bank bank);
	List<BankAccount> findByDate(Date date);
	BankAccount findByNumber(String number);
	

	@Query("select b FROM BankAccount b where   b.id!=:id  and b.currency.name = :curency")
	
	List<BankAccount> findBankAccountForDelet(int id,String curency);
	



		
}
