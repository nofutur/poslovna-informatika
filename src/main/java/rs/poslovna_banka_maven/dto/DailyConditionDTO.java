package rs.poslovna_banka_maven.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import rs.poslovna_banka_maven.entity.BankAccount;
import rs.poslovna_banka_maven.entity.DailyCondition;

public class DailyConditionDTO {

	private int id;
	
	private double previousState;
	
	private double newState;
	
	private BankAccount bankAccount;

	public DailyConditionDTO(DailyCondition d) {
		super();
		this.id = d.getId();
		this.previousState = d.getPreviousState();
		this.newState = d.getNewState();
		this.bankAccount=d.getBankAccount();

	}

	public DailyConditionDTO() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPreviousState() {
		return previousState;
	}

	public void setPreviousState(double previousState) {
		this.previousState = previousState;
	}

	public double getNewState() {
		return newState;
	}

	public void setNewState(double newState) {
		this.newState = newState;
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bills) {
		this.bankAccount = bills;
	}
	
}




