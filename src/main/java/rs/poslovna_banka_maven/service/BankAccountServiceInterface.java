package rs.poslovna_banka_maven.service;

import java.util.Date;
import java.util.List;

import rs.poslovna_banka_maven.dto.BankDTO;
import rs.poslovna_banka_maven.dto.CurrencyDTO;
import rs.poslovna_banka_maven.dto.BankAccountDTO;
import rs.poslovna_banka_maven.dto.UserDTO;
import rs.poslovna_banka_maven.entity.Bank;
import rs.poslovna_banka_maven.entity.BankAccount;
import rs.poslovna_banka_maven.entity.Currency;
import rs.poslovna_banka_maven.entity.User;

public interface BankAccountServiceInterface {

	public List<BankAccountDTO> findAll();
	public BankAccountDTO findById(int id);

	public List<BankAccountDTO> findBankAccountForDelet(Integer id);
	public List<BankAccountDTO> findByUser(UserDTO user);
	public List<BankAccountDTO> findByBank(BankDTO bank);
	public List<BankAccountDTO> findByDate(Date date);
	public BankAccount convert( String number1 ,int balance1,Bank  bank,User user,Currency currency);
	public BankAccountDTO findByName(String name);
	public BankAccountDTO findByNumber(String number);
	public void save(BankAccount ba);
	public void delete(int id);
	public List<BankAccountDTO> findByUser(User user) ;
	public void delete(Integer idBankAccount,Integer idBankAccountForDelete);
	public BankAccount  DTOToBankAccount (BankAccountDTO transakcijeDTO);
}
