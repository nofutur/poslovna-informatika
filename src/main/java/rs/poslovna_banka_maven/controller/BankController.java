package rs.poslovna_banka_maven.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rs.poslovna_banka_maven.dto.BankDTO;
import rs.poslovna_banka_maven.service.BankService;

@RestController
@RequestMapping("/api/banks/")
public class BankController {

	@Autowired
	public BankService bankService;
	
	@GetMapping()
	public ResponseEntity<List<BankDTO>> getBanks(){
		System.err.println("-BankService-");
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentUserName = "";
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
		    currentUserName = authentication.getName();
		    System.out.println(currentUserName);
		}
		
        List<BankDTO> banksDTO = bankService.findAll();
//		System.err.println(banksDTO);

        return new ResponseEntity<>(banksDTO,HttpStatus.OK);
	}
	
	@GetMapping(path=("{username}"))
	public ResponseEntity<BankDTO> getBankByUsername(@RequestParam String username){
		System.err.println("-BankServicegetByUSername-");
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentUserName = "";
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
		    currentUserName = authentication.getName();
		    System.out.println(currentUserName);
		}
		
        BankDTO bankDTO = bankService.findByUsername(username);
//		System.err.println(banksDTO);

        return new ResponseEntity<>(bankDTO,HttpStatus.OK);
	}
}
