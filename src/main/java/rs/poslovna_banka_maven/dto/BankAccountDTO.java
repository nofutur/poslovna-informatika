package rs.poslovna_banka_maven.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


import rs.poslovna_banka_maven.entity.BankAccount;
import rs.poslovna_banka_maven.entity.Currency;
import rs.poslovna_banka_maven.entity.DailyCondition;

public class BankAccountDTO {

    @Override
	public String toString() {
		return "BankAccountDTO [id=" + id + ", number=" + number + ", date=" + date + ", balance=" + balance
				+ ", valid=" + valid + ", dailyConditions=" + dailyConditions + ", currency=" + currency + ", bank="
				+ bank + ", user=" + user + "]";
	}

	private int id;
	
	private String number;

	private Date date;

	private double balance;
	
	private boolean valid;
	
	private List<DailyConditionDTO> dailyConditions;
	
	private CurrencyDTO currency;
	
	private BankDTO bank;
	

	private UserDTO user;

	public BankDTO getBank() {
		return bank;
	}
	
	public void setBank(BankDTO bank) {
		this.bank = bank;
	}
	
	public UserDTO getUser() {
		return user;
	}
	
	public void setUser(UserDTO user) {
		this.user = user;
	}

	
	public BankAccountDTO(BankAccount bankAccount) {
		super();
		this.id = bankAccount.getId();
		this.number = bankAccount.getNumber();
		this.date = bankAccount.getDate();
		this.balance = bankAccount.getBalance();
		this.valid = bankAccount.isValid();
		dailyConditions = new ArrayList<DailyConditionDTO>();
		if (bankAccount.getDailyConditions() != null) {
			for (DailyCondition dailyCondition : bankAccount.getDailyConditions()) {
				dailyConditions.add(new DailyConditionDTO(dailyCondition));
			}
		}
		CurrencyDTO currencyDTO = (bankAccount.getCurrency() != null)? new CurrencyDTO(bankAccount.getCurrency()) : null;
		this.currency = currencyDTO;
//		this.bank = new BankDTO(bankAccount.getBank());
//		this.user = new UserDTO(bankAccount.getUser());
	}

	public BankAccountDTO(int id, String number, Date date, double balance, boolean valid,
			List<DailyConditionDTO> dailyConditions, CurrencyDTO currency) {
		super();
		this.id = id;
		this.number = number;
		this.date = date;
		this.balance = balance;
		this.valid = valid;
		this.dailyConditions = dailyConditions;
		this.currency = currency;
	}

	public BankAccountDTO() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public List<DailyConditionDTO> getDailyConditions() {
		return dailyConditions;
	}

	public void setDailyConditions(List<DailyConditionDTO> dailyConditions) {
		this.dailyConditions = dailyConditions;
	}

	public CurrencyDTO getCurrency() {
		return currency;
	}

	public void setCurrency(CurrencyDTO currency) {
		this.currency = currency;
	}
	
	
}

