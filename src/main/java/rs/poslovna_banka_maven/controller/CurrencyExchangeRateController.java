package rs.poslovna_banka_maven.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.poslovna_banka_maven.dto.CurrencyExchangeRateDTO;
import rs.poslovna_banka_maven.entity.Currency;
import rs.poslovna_banka_maven.entity.CurrencyExchangeRate;
import rs.poslovna_banka_maven.entity.ExchangeRateList;
import rs.poslovna_banka_maven.service.CurrencyExchangeRateService;
import rs.poslovna_banka_maven.service.CurrencyService;

@RestController
@RequestMapping("api/currencyExchangeRate")
public class CurrencyExchangeRateController {

	@Autowired
	CurrencyExchangeRateService currencyExchangeRateservice;
	@Autowired
	CurrencyService cs;
	@GetMapping
	public ResponseEntity<List<CurrencyExchangeRateDTO>> getAll() {
		List<CurrencyExchangeRate> cerList = currencyExchangeRateservice.getAll();
		List<CurrencyExchangeRateDTO> cerDTO = new ArrayList<CurrencyExchangeRateDTO>();
		for(CurrencyExchangeRate cer : cerList) {
			cerDTO.add(new CurrencyExchangeRateDTO(cer));
		}
		return new ResponseEntity<>(cerDTO, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<CurrencyExchangeRateDTO> getCurrencyExchangeRate(@PathVariable Integer id){
		CurrencyExchangeRate cer = currencyExchangeRateservice.get(id);
		if(cer == null){
			return new ResponseEntity<CurrencyExchangeRateDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<CurrencyExchangeRateDTO>(new CurrencyExchangeRateDTO(cer), HttpStatus.OK);
	}
	
	@PostMapping(path = "/addNew",consumes = "application/json")
	public ResponseEntity<CurrencyExchangeRate> insert(@RequestBody CurrencyExchangeRateDTO newCurrencyExchange){
		
		CurrencyExchangeRate cer = new CurrencyExchangeRate();
		cer.setOrdinalNumber(newCurrencyExchange.getOrdinalNumber());
		cer.setBuying(newCurrencyExchange.getBuying());
		cer.setMiddle(newCurrencyExchange.getMiddle());
		cer.setSelling(newCurrencyExchange.getSelling());
//		Currency cur = cs.findByCodeOfCurrency(newCurrencyExchange.getCurrency().getName());
//		cer.setCurrency(cur);
//		cer.setExchangeRateList(new ExchangeRateList());
		
		
//		CurrencyExchangeRate cer=new CurrencyExchangeRate();
//		cer.setOrdinalNumber(cerDTO.getOrdinalNumber());
//		cer.setBuying(cerDTO.getBuying());
//		cer.setMiddle(cerDTO.getMiddle());
//		cer.setSelling(cerDTO.getSelling());
//		cer.setCurrency(cerDTO.getCurrency());
//		cer.setExchangeRateList(cerDTO.getExchangeRateList());
		currencyExchangeRateservice.insert(cer);
		return new ResponseEntity<CurrencyExchangeRate>(HttpStatus.OK);	
	}
	
	@PutMapping(path = "/{id}", consumes = "application/json")
	public ResponseEntity<CurrencyExchangeRateDTO> edit(@PathVariable int id,@RequestBody CurrencyExchangeRateDTO cerDTO){
		CurrencyExchangeRate cer = currencyExchangeRateservice.get(id);
		if(cer == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		else {
			cer.setOrdinalNumber(cerDTO.getOrdinalNumber());
			cer.setBuying(cerDTO.getBuying());
			cer.setMiddle(cerDTO.getMiddle());
			cer.setSelling(cerDTO.getSelling());
			currencyExchangeRateservice.insert(cer);	
			return new ResponseEntity<CurrencyExchangeRateDTO>(new CurrencyExchangeRateDTO(cer), HttpStatus.OK);
		}
	}
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id){
		CurrencyExchangeRate cer = currencyExchangeRateservice.get(id);
		if (cer != null){
			currencyExchangeRateservice.delete(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {		
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}
	
}
