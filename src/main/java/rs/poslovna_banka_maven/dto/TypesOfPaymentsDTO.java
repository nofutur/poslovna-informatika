package rs.poslovna_banka_maven.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import rs.poslovna_banka_maven.entity.Transakcije;
import rs.poslovna_banka_maven.entity.TypesOfPayments;

public class TypesOfPaymentsDTO {

	private int id;
	
	private String paymentTypeCode;
	
	private String name;

	public TypesOfPaymentsDTO() {
		super();
	}

	public TypesOfPaymentsDTO(TypesOfPayments t) {
		this.id = t.getId();
		this.paymentTypeCode = t.getPaymentTypeCode();
		this.name = t.getName();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPaymentTypeCode() {
		return paymentTypeCode;
	}

	public void setPaymentTypeCode(String paymentTypeCode) {
		this.paymentTypeCode = paymentTypeCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}