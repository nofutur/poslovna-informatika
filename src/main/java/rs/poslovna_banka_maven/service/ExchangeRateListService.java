package rs.poslovna_banka_maven.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.poslovna_banka_maven.dto.BankDTO;
import rs.poslovna_banka_maven.dto.ExchangeRateListDTO;
import rs.poslovna_banka_maven.entity.CurrencyExchangeRate;
import rs.poslovna_banka_maven.entity.ExchangeRateList;
import rs.poslovna_banka_maven.repository.BankRepository;
import rs.poslovna_banka_maven.repository.ExchangeRateListRepository;

@Service
public class ExchangeRateListService implements ExchangeRateListServiceInterface {
	
	@Autowired
	private ExchangeRateListRepository erlr;
	
	
	@Autowired
	private BankRepository bankRepository;
	
	@Override
	public List<ExchangeRateListDTO> findAll() {
		List<ExchangeRateListDTO> erlist = new ArrayList<ExchangeRateListDTO>();
		List<ExchangeRateList> erl = erlr.findAll();
		
		for(ExchangeRateList erl1 : erl) {
			ExchangeRateListDTO e = new ExchangeRateListDTO(erl1);
			e.setBank(new BankDTO (erl1.getBank()));
			erlist.add(e);
		}
		return erlist;
	}


	@Override
	public ExchangeRateListDTO getOne(Integer id) {
		ExchangeRateList erl = erlr.getOne(id);
		ExchangeRateListDTO e =  new ExchangeRateListDTO(erl);
		e.setBank(new BankDTO (erl.getBank()));
		return e;
	}
	
	@Override
    public ExchangeRateList get(Integer id) {
    	Optional<ExchangeRateList> ExchangeRateList = erlr.findById(id);
        return ExchangeRateList.get();
    }
	
	@Override
	public ExchangeRateListDTO save(ExchangeRateListDTO erl) {
		ExchangeRateList e = new ExchangeRateList(erl);
	
		
		
		e.setDate(erl.getDate());
		e.setAppliedBy(erl.getAppliedBy());
		e.setId(erl.getId());
		e.setNumberOfExchangeRateList(erl.getNumberOfExchangeRateList());
	
		e.setBank(bankRepository.findByUsername(erl.getBank().getUsername()));
		
		
		
		System.err.println("save..."+erl.getBank().getId());
		
		ExchangeRateListDTO newerl = new ExchangeRateListDTO( erlr.save(e));
	
		return newerl;
		
	}

	
	@Override
	public void delete(Integer id) {
		erlr.deleteById(id);
		System.out.println("deleted " + id);
	}
}
