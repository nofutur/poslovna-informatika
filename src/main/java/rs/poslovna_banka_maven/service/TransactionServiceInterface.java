package rs.poslovna_banka_maven.service;


import java.util.Date;
import java.util.List;

import rs.poslovna_banka_maven.dto.TransakcijeDTO;

import rs.poslovna_banka_maven.entity.Transakcije;
public interface TransactionServiceInterface {

	public List<TransakcijeDTO> searchTransactionBetweenDate(Date d1,Date d2);
	public List<TransakcijeDTO> getAll(Date date1,Date date2,String name);
	public void  save(Transakcije transakcije);
	public List<Transakcije> findByCreditorContains(String a);
	public TransakcijeDTO transactionToDTO (Transakcije transacton);
	public Transakcije  DTOToTransakcije (TransakcijeDTO transakcijeDTO);
	public List<TransakcijeDTO> findByDebitor(String debitor,String name);
	List<TransakcijeDTO> findByRecipient(String debitor, String name);

}
