package rs.poslovna_banka_maven.service;

import java.util.List;

import rs.poslovna_banka_maven.entity.Message;

public interface MessageServiceInterface {
	List<Message> getAll();
	Message get(Integer id);
	Message insert(Message country);
	Message edit(Message country);
	void delete(Integer id);
}
