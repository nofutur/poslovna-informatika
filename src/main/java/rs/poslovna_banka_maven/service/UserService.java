package rs.poslovna_banka_maven.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import rs.poslovna_banka_maven.dto.BankAccountDTO;
import rs.poslovna_banka_maven.dto.BankDTO;
import rs.poslovna_banka_maven.dto.UserDTO;
import rs.poslovna_banka_maven.entity.Bank;
import rs.poslovna_banka_maven.entity.BankAccount;
import rs.poslovna_banka_maven.entity.Currency;
import rs.poslovna_banka_maven.entity.User;
import rs.poslovna_banka_maven.repository.BankAccountRepository;
import rs.poslovna_banka_maven.repository.BankRepository;
import rs.poslovna_banka_maven.repository.CurrencyRepository;
import rs.poslovna_banka_maven.repository.UserRepository;
@Service
public class UserService implements UserServiceInterface {

	
	
	@Autowired 
	UserRepository  userRepository;
	@Autowired 
	BankRepository bankRepository;
	
	@Autowired 
	CurrencyRepository CurrencyRepository;
	
	
	@Autowired 
	BankAccountRepository BankAccountRepository;
	
	@Override
	public void save(User user) {
		userRepository.save(user);	
	} 
	
	@Override
	public UserDTO editUser(UserDTO userDTO) {
		
		User user = userRepository.getOne(userDTO.getId());
		if (user == null) {
			return null;			
		}
		System.err.println("----getAll by bank username--22--"+userDTO.toString());

		System.err.println("sdsd--"+userDTO.getId());
		user.setAddress(userDTO.getAddress());
		user.setFirstname(userDTO.getFirstname());
		user.setIdentificationNumber(userDTO.getIdentificationNumber());
		user.setLastname(userDTO.getLastname());
		user.setPhoneNumber(userDTO.getPhoneNumber());
		user.setUsername(userDTO.getUsername());
		userRepository.save(user);
		
		return userDTO;
	}
	
	@Override
	public UserDTO getUserById(int id) {
		User user =userRepository.getOne(id);
		
		
		UserDTO userDTO = new UserDTO();
	
		userDTO.setAddress(user.getAddress());
	//	userDTO.setBankAccounts(user.getBankAccounts());
		userDTO.setFirstname(user.getFirstname());
		userDTO.setId(user.getId());
		userDTO.setIdentificationNumber(user.getIdentificationNumber());
		userDTO.setLastname(user.getLastname());
		userDTO.setLegalEntity(user.getLegalEntity());
		userDTO.setPhoneNumber(user.getPhoneNumber());
		userDTO.setUsername(user.getUsername());
		return userDTO;
		
		
	}
	
	@Override
	public void delete(int id) {
		
		userRepository.deleteById(id);
	}
	
	@Override
	public UserDTO findByUsername(String username) {
		UserDTO user = new UserDTO(userRepository.findByUsername(username));
		return user;
	}
	
	@Override
	public List<UserDTO> findByBankAndLegalEntity(int BankId, boolean legalEntity) {
		List<User> users = userRepository.findByBankAndLegalEntity(BankId, legalEntity);
		List<UserDTO> usersDTO = new ArrayList<UserDTO>();
		for(User u : users) {
			UserDTO user = new UserDTO(u);
			usersDTO.add(user);
		}
		return usersDTO;
	}

	@Override
	public List<UserDTO> findByBank(BankDTO bank) {
		List<BankAccountDTO> baDTO =  bank.getBankAccounts();
		Set<UserDTO> usersDTO = new HashSet<>();
		for(BankAccountDTO b : baDTO) {
			usersDTO.add(b.getUser());
		}
		return (List<UserDTO>) usersDTO;
	}

	@Override
	public UserDTO save(UserDTO userDTO) {
	
		
		Bank bank = bankRepository.findById(1).orElse(null);

		
		User user = new User();
//		User user = userRepository.getOne(userDTO.getId());
//		if (user == null) {
//			return null;			
//		}
		System.err.println("----getAll by bank username--22--"+userDTO.toString());

//		System.err.println("sdsd--"+userDTO.getId());
		user.setAddress(userDTO.getAddress());
		user.setFirstname(userDTO.getFirstname());
		user.setIdentificationNumber(userDTO.getIdentificationNumber());
		user.setLastname(userDTO.getLastname());
		user.setPhoneNumber(userDTO.getPhoneNumber());
		user.setUsername(userDTO.getUsername());
		user.setActivated(true);
		user.setPassword("$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi");
		user.setLegalEntity(false);
//		user.setBankAccounts((Set<BankAccount>) BankAccountRepository.findBy);
		
		
		BankAccount bankAccount = new BankAccount();
		bankAccount.setBalance(0);
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
		Date date = new Date(System.currentTimeMillis());
		formatter.format(date);
		bankAccount.setDate(date);
		
		
		
		int number1 = 100 + (int)(Math.random() * ((999 - 100) + 1));
		int number2 = 10 + (int)(Math.random() * ((99 - 10) + 1));
		int number3 = 100000 + (int)(Math.random() * ((999999 - 100000) + 1));
		
		
		String number = number1 + "-1000000" + number3 + "-" + number2;
		bankAccount.setNumber(number);
		bankAccount.setValid(true);
		user.addBankAccount(bankAccount);
		bank.addBankAccount(bankAccount);
		
		
		//dodaj valutu 
		Currency currency = (Currency) CurrencyRepository.findById(1).orElse(null);
		currency.addBankAccount(bankAccount);
		
		
		
		
		
		
		
		
		
		userRepository.save(user);
		
		return userDTO;
		
		
	}

}
