package rs.poslovna_banka_maven.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.poslovna_banka_maven.dto.MessageDTO;
import rs.poslovna_banka_maven.dto.TransakcijeDTO;

@Entity
@Table(name = "messages")
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "message_id", unique = true, nullable = false)
	private int id;
	
	@Column(name = "message_type")
	@Enumerated(EnumType.STRING)
	private TypesOfMessages type;
	
	@Column(name = "amount")
	private double amount;
	
	@Column(name = "code_of_value")
	private String codeOfValue;
	
	@Column(name = "date_of_value")
	private Date dateOfValue;
	
	@Column(name = "message_date")
	private Date date;
	
	@ManyToOne
	@JoinColumn(name="bank_id_debtor", referencedColumnName="bank_id", nullable=false)
	public Bank debtorBank;
	
	
	@OneToMany(cascade= {ALL}, fetch=LAZY, mappedBy = "message")
	public Set<Transakcije> transactions = new HashSet<Transakcije>();
	
	public Message() {
	}
	
	public Message(int id, TypesOfMessages type, double amount, String codeOfValue, Date dateOfValue, Date date,
			Bank debtorBank, Set<Transakcije> transactions) {
		super();
		this.id = id;
		this.type = type;
		this.amount = amount;
		this.codeOfValue = codeOfValue;
		this.dateOfValue = dateOfValue;
		this.date = date;
		this.debtorBank = debtorBank;
		this.transactions = transactions;
	}
	
	public Message(TypesOfMessages type, double amount, String codeOfValue, Date dateOfValue, Date date,
			Bank debtorBank) {
		this.type = type;
		this.amount = amount;
		this.codeOfValue = codeOfValue;
		this.dateOfValue = dateOfValue;
		this.date = date;
		this.debtorBank = debtorBank;
	}
	
	public Message(MessageDTO m) {
		this.id = m.getId();
		this.type = m.getType();
		this.amount = m.getAmount();
		this.codeOfValue = m.getCodeOfValue();
		this.dateOfValue = m.getDateOfValue();
		this.date = m.getDate();
		this.debtorBank = m.getDebtorBank();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TypesOfMessages getType() {
		return type;
	}

	public void setType(TypesOfMessages type) {
		this.type = type;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCodeOfValue() {
		return codeOfValue;
	}

	public void setCodeOfValue(String codeOfValue) {
		this.codeOfValue = codeOfValue;
	}

	public Date getDateOfValue() {
		return dateOfValue;
	}

	public void setDateOfValue(Date dateOfValue) {
		this.dateOfValue = dateOfValue;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public Bank getDebtorBank() {
		return debtorBank;
	}

	public void setDebtorBank(Bank debtorBank) {
		this.debtorBank = debtorBank;
	}

	public Set<Transakcije> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<Transakcije> transactions) {
		this.transactions = transactions;
	}
}
