package rs.poslovna_banka_maven.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rs.poslovna_banka_maven.dto.BankDTO;
import rs.poslovna_banka_maven.dto.UserDTO;
import rs.poslovna_banka_maven.entity.Bank;
import rs.poslovna_banka_maven.entity.User;
import rs.poslovna_banka_maven.service.BankServiceInterface;
import rs.poslovna_banka_maven.service.TransactionServiceInterface;
import rs.poslovna_banka_maven.service.UserServiceInterface;

@RestController
@RequestMapping("/api/users")
public class UsersController {

	@Autowired
	BankServiceInterface bankServiceInterface;

	@Autowired
	UserServiceInterface userServiceInterface;


	
	@PostMapping(consumes = "application/json",path = "/addUser", produces = "application/json")
	public ResponseEntity<UserDTO> editNewUser(@RequestBody UserDTO userDTO, Principal principal) {

		System.out.println("novo");
		

		if (userDTO == null) {
			return new ResponseEntity<UserDTO>(HttpStatus.BAD_REQUEST);
		}

		UserDTO user = userServiceInterface.save(userDTO);
		// treba doraditi
		if (user == null) {
			return new ResponseEntity<UserDTO>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<UserDTO>(user,HttpStatus.OK);
	}
	
	
	@GetMapping("/legalEntity/{legalEntity}")
	public ResponseEntity<List<UserDTO>> getAll(Principal principal, @PathParam(value = "legalEntity") boolean legalEntity){
		System.out.println("legalEntity"+legalEntity);
		System.out.println("legalEntity"+principal.getName());

		BankDTO bankDto = bankServiceInterface.findByUsername("aaleksic");
		System.out.println("legalEntity"+bankDto);

		if (bankDto == null) {
			System.out.println("BAD_REQUEST");

			return new ResponseEntity<List<UserDTO>>(HttpStatus.NOT_FOUND);
		}
		List<UserDTO> users = userServiceInterface.findByBankAndLegalEntity(bankDto.getId(), legalEntity);
		if (users == null) {
			return new ResponseEntity<List<UserDTO>>(HttpStatus.NOT_FOUND);
		}
		

		return new ResponseEntity<List<UserDTO>>(users, HttpStatus.OK);
	}
	
	
	@GetMapping("/{bank}")
	public ResponseEntity<List<UserDTO>> getAllByBank(Principal principal, @PathParam(value = "bank") String bankUsername){
		System.err.println("----getAll by bank username----");
		BankDTO bank = bankServiceInterface.findByUsername(bankUsername);
		if (bank == null) {
			return new ResponseEntity<List<UserDTO>>(HttpStatus.BAD_REQUEST);
		}
		
		List<UserDTO> users = userServiceInterface.findByBank(bank);
		return new ResponseEntity<List<UserDTO>>(users, HttpStatus.OK);
	}

	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<UserDTO> editUser(@RequestBody UserDTO userDTO,
			@RequestParam(name = "currencyId") int currencyId) {

		

		if (userDTO == null) {
			return new ResponseEntity<UserDTO>(HttpStatus.BAD_REQUEST);
		}

		UserDTO user = userServiceInterface.editUser(userDTO);
		// treba doraditi
		if (user == null) {
			return new ResponseEntity<UserDTO>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<UserDTO>(user,HttpStatus.OK);
	}
  
	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<UserDTO> getUser(@PathVariable("id") int id) {
		System.err.println("novi problemm" + id);
		UserDTO userDTO = userServiceInterface.getUserById(id);

		return new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);
	}


	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> deleteUser(@PathVariable("id") int id) {
		System.err.println("delete by id");
		UserDTO userDTO = userServiceInterface.getUserById(id);
		
		if (userDTO == null) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		
		userServiceInterface.delete(id);
		return new ResponseEntity<Void>(HttpStatus.OK); 
	}

}
