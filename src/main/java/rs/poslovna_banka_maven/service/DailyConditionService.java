package rs.poslovna_banka_maven.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.poslovna_banka_maven.entity.DailyCondition;
import rs.poslovna_banka_maven.repository.DailyConditionRepository;
@Service
public class DailyConditionService  implements DailyConditionServiceInterface {
	@Autowired
	DailyConditionRepository repo;

	@Override
	public List<DailyCondition> findAll() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public DailyCondition findById(int id) {
		// TODO Auto-generated method stub
		DailyCondition dailyCondition = repo.findById(id);
		if(dailyCondition==null) {
			return null;
		}
		return dailyCondition;
	}

	@Override
	public DailyCondition save(DailyCondition d) {
		// TODO Auto-generated method stub
		return repo.save(d);
	}

	@Override
	public void delete(DailyCondition d) {
		// TODO Auto-generated method stub
		repo.delete(d);
		
	}

}
