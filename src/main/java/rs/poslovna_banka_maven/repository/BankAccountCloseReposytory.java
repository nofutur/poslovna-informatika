package rs.poslovna_banka_maven.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rs.poslovna_banka_maven.entity.BankAccountClose;



@Repository
public interface BankAccountCloseReposytory extends JpaRepository<BankAccountClose, Integer> {
	
	public BankAccountClose save(BankAccountClose  bankAccountClose);
	
}
