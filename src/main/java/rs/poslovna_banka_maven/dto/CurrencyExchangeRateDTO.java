package rs.poslovna_banka_maven.dto;

import rs.poslovna_banka_maven.entity.Currency;
import rs.poslovna_banka_maven.entity.CurrencyExchangeRate;
import rs.poslovna_banka_maven.entity.ExchangeRateList;

public class CurrencyExchangeRateDTO {

	private int id;
	private int ordinalNumber;
	private double buying;
	private double middle;
	private double selling;
	private Currency currency;
	private ExchangeRateList exchangeRateList;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getOrdinalNumber() {
		return ordinalNumber;
	}
	public void setOrdinalNumber(int ordinalNumber) {
		this.ordinalNumber = ordinalNumber;
	}
	public double getBuying() {
		return buying;
	}
	public void setBuying(double buying) {
		this.buying = buying;
	}
	public double getMiddle() {
		return middle;
	}
	public void setMiddle(double middle) {
		this.middle = middle;
	}
	public double getSelling() {
		return selling;
	}
	public void setSelling(double selling) {
		this.selling = selling;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public ExchangeRateList getExchangeRateList() {
		return exchangeRateList;
	}
	public void setExchangeRateList(ExchangeRateList exchangeRateList) {
		this.exchangeRateList = exchangeRateList;
	}
	
	public CurrencyExchangeRateDTO(CurrencyExchangeRate cer) {
		this.id = cer.getId();
		this.ordinalNumber = cer.getOrdinalNumber();
		this.buying = cer.getBuying();
		this.middle = cer.getMiddle();
		this.selling = cer.getSelling();
		this.currency = cer.getCurrency();
		this.exchangeRateList = cer.getExchangeRateList();
	}
	
	public CurrencyExchangeRateDTO() {
		
	}
}
