package rs.poslovna_banka_maven.dto;

import java.util.Date;

import rs.poslovna_banka_maven.entity.Bank;
import rs.poslovna_banka_maven.entity.*;

public class ERLCreateDTO {

	private Date date;
	private int numberOfExchangeRateList;
	private Date appliedBy;
	private BankDTO bank;
	
	public ERLCreateDTO(Date date, int numberOfExchangeRateList, Date appliedBy, BankDTO bank) {

		this.date = date;
		this.numberOfExchangeRateList = numberOfExchangeRateList;
		this.appliedBy = appliedBy;
		this.bank = bank;
	}
	
	public ERLCreateDTO(ExchangeRateList erl ) {
		this.date = erl.getDate();
		this.numberOfExchangeRateList = erl.getNumberOfExchangeRateList();
		this.appliedBy = erl.getAppliedBy();
		BankDTO b = new BankDTO(erl.getBank());
		this.bank = b;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getNumberOfExchangeRateList() {
		return numberOfExchangeRateList;
	}

	public void setNumberOfExchangeRateList(int numberOfExchangeRateList) {
		this.numberOfExchangeRateList = numberOfExchangeRateList;
	}

	public Date getAppliedBy() {
		return appliedBy;
	}

	public void setAppliedBy(Date appliedBy) {
		this.appliedBy = appliedBy;
	}

	public BankDTO getBank() {
		return bank;
	}

	public void setBank(BankDTO bank) {
		this.bank = bank;
	}
}
