package rs.poslovna_banka_maven.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import rs.poslovna_banka_maven.dto.BankAccountDTO;
import rs.poslovna_banka_maven.dto.CurrencyExchangeRateDTO;
import rs.poslovna_banka_maven.dto.DailyAccountBalanceDTO;
import rs.poslovna_banka_maven.dto.ModelDTO;
//import rs.poslovna_banka_maven.dto.m
import rs.poslovna_banka_maven.dto.TransakcijeDTO;
import rs.poslovna_banka_maven.dto.UserDTO;
import rs.poslovna_banka_maven.entity.Model;
import rs.poslovna_banka_maven.entity.PopulatedPlace;
import rs.poslovna_banka_maven.entity.Transakcije;
import rs.poslovna_banka_maven.service.BankAccountServiceInterface;
import rs.poslovna_banka_maven.service.BankServiceInterface;
import rs.poslovna_banka_maven.service.DailyConditionServiceInterface;
import rs.poslovna_banka_maven.service.MessageServiceInterface;
import rs.poslovna_banka_maven.service.PopulatedPlaceInterface;
import rs.poslovna_banka_maven.service.TransactionServiceInterface;
import rs.poslovna_banka_maven.service.TypesOfPaymentsInterface;
import rs.poslovna_banka_maven.service.UserServiceInterface;
import rs.poslovna_banka_maven.entity.BankAccount;
import rs.poslovna_banka_maven.entity.Currency;
import rs.poslovna_banka_maven.entity.CurrencyExchangeRate;
import rs.poslovna_banka_maven.entity.DailyCondition;
import rs.poslovna_banka_maven.entity.Message;
import rs.poslovna_banka_maven.entity.TypesOfPayments;
import rs.poslovna_banka_maven.entity.User;
import rs.poslovna_banka_maven.repository.BankAccountRepository;
import rs.poslovna_banka_maven.repository.UserRepository;

@RestController
@RequestMapping("/api/transactions")
public class TransactionController {

	@Autowired
	BankAccountServiceInterface bankAccounServiceInterface;

	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	public static final String XML_FILE_PATH = "C:\\lucine\\BankXml\\";
	private static String call = "http://localhost:8082/api/narodna_Bank/addModel";
	public static String upladDirectory = System.getProperty("user.dir") + "/upload";

	DocumentBuilder dBuilder;
	 private static final String DEFAULT_FILE_NAME = "Transakcije.xml";
	 
	    @Autowired
	    private ServletContext servletContext;
	@Autowired
	UserServiceInterface userServiceInterface;
	
	@Autowired
	UserRepository userRepository;


	@Autowired
	TransactionServiceInterface transactionServiceInterface;

	@Autowired
	BankServiceInterface bankServiceInterface;

	@Autowired
	DailyConditionServiceInterface dailyConditionServiceInterface;

	@Autowired
	BankAccountServiceInterface bankAccountServiceInterface;
	
	@Autowired
	BankAccountRepository bankAccountRepository;
	@Autowired
	PopulatedPlaceInterface populatedPlaceInterface;
	@Autowired
	TypesOfPaymentsInterface typesOfPaymentsInterface;

	@Autowired
	MessageServiceInterface messageServiceInterface;

	
	
	
	@GetMapping(path = "/{vremePocetno}/{vremeKrajnje}")
	public ResponseEntity<List<TransakcijeDTO>> getTransactions(@PathVariable String vremePocetno,
			@PathVariable String vremeKrajnje, Principal principal) throws ParseException {
		System.err.println("----------principal-----22-----" + principal.getName());

		System.out.println("vremePocetno" + vremePocetno);
		System.out.println("vremeKrajnje" + vremeKrajnje);

		
		
		
		System.out.println(vremePocetno + vremeKrajnje);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date d1 = format.parse(vremePocetno);
		Date d2 = format.parse(vremeKrajnje);
		System.out.println(d1 + "s" + d2);
		
		
		
		
		return new ResponseEntity<>(transactionServiceInterface.getAll(d1,d2,principal.getName()), HttpStatus.OK);

	}
	
	@GetMapping(path = "/{brojDuznikaText}")
	public ResponseEntity<List<TransakcijeDTO>> findByDebitor(@PathVariable String brojDuznikaText,
			 Principal principal) throws ParseException {
		System.out.println("TransactionController.findByDebitor()");
		System.out.println("brojDuznikaText" + brojDuznikaText);
		


		
		
		
		return new ResponseEntity<>(transactionServiceInterface.findByDebitor(brojDuznikaText,principal.getName()), HttpStatus.OK);

	}

	
	
	@GetMapping(path = "/findByRecipient/{brojPrimaocaText}")
	public ResponseEntity<List<TransakcijeDTO>> findByRecipient(@PathVariable String brojPrimaocaText,
			 Principal principal) throws ParseException {
		System.out.println("TransactionController.findByDebitor()");
		System.out.println("brojDuznikaText" + brojPrimaocaText);
		
		
		return new ResponseEntity<>(transactionServiceInterface.findByRecipient(brojPrimaocaText,principal.getName()), HttpStatus.OK);

	}

	
	
	
	
	
	
	
	
	
	
	
	@PostMapping(path = "/addModel")
	public void getBank(@RequestBody ModelDTO modelDTO) {

		System.err.println("ModelDTO.   " + modelDTO.getAmount());

	}

	@PostMapping(path = "/", consumes = "application/json")
	public ResponseEntity<TransakcijeDTO> addInterBankTransver(@RequestBody ModelDTO modelDTO, Principal principal) {

		System.err.println("----------principal----1------" + modelDTO.getNumberRecipient());

		BankAccountDTO bankAccauntRecipient = bankAccountServiceInterface.findByNumber(modelDTO.getNumberRecipient());

		System.err.println("----------principal-----2-----" + bankAccauntRecipient.getBalance());
		System.err.println("----------principal-----3-----" + modelDTO.getAmount());

		if (bankAccauntRecipient.getBalance() < modelDTO.getAmount()) {

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		BankAccountDTO bankAccauntDebtor = bankAccountServiceInterface.findByNumber(modelDTO.getNumberDebtor());

		TransakcijeDTO tr = new TransakcijeDTO();

		if (bankAccauntDebtor != null) {
			System.out.println("unutrasnji prenos");

			tr = transakcija(modelDTO, principal.getName());
			return new ResponseEntity<TransakcijeDTO>(tr, HttpStatus.OK);

		}

		else {
			System.out.println("medjubankarski prenos");
			tr = medjubankarskiPrenos(modelDTO, principal);
			return new ResponseEntity<>(tr, HttpStatus.OK);
		}

//		BankAccount bankAccRec=bankAccountServiceInterface.getByNumber(modelDTO.getNumberRecipient());
//
//	
//	
//		return new ResponseEntity<>("uspeo", HttpStatus.OK);

	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<String> deleteTransaction(@PathVariable int id) {

		System.out.println("delete transaction" + id);

		return new ResponseEntity<>("uspeo", HttpStatus.OK);

	}

//	@GetMapping( path = "/izvodPDF/{vremePocetno}/{vremeKrajnje}")
//	public ResponseEntity<String> izvod(@PathVariable String vremePocetno,@PathVariable String vremeKrajnje){
//	
//		
//		System.out.println("izvod");
//		
//		System.out.println("vremePocetno"+vremePocetno);
//		System.out.println("vremeKrajnje"+vremeKrajnje);
//
//		return new ResponseEntity<>("uspeo", HttpStatus.OK);
//
//}

	public TransakcijeDTO transakcija(ModelDTO model, String principal) {

		System.err.println("--------transakcija----------");
		System.err.println("--------transakcija----------" + model.getNumberDebtor());
		System.err.println("--------transakcija----------" + model.getNumberRecipient());

		Set<Currency> currencies = new HashSet<Currency>();
		// currencies

		DailyCondition dailyCondition = dailyConditionServiceInterface.findById(1);
		PopulatedPlace place = populatedPlaceInterface.get(1);
		TypesOfPayments types = typesOfPaymentsInterface.get(1);
		Message m = messageServiceInterface.get(1);

		// accounts
//		User u=userServiceInterface.findByUsername(principal);

		System.currentTimeMillis();

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date date = new Date(System.currentTimeMillis());
		// formatter.format(date);
//		
//		//Account
		BankAccountDTO bankAccRec = bankAccountServiceInterface.findByNumber(model.getNumberRecipient());

		System.err.println("--------transakcija-----1-----" + bankAccRec.getId());
		String uplata;
		if (bankAccRec.getBalance() < model.getAmount()) {
			return null;
		}
		bankAccRec.setBalance(bankAccRec.getBalance() - model.getAmount());

		BankAccountDTO bankAccDeb = bankAccountServiceInterface.findByNumber(model.getNumberDebtor());

		System.out.println("bankAccDeb.getBalance---" + bankAccDeb.getBalance());
		System.out.println("model.getAmount---" + model.getAmount());

		bankAccDeb.setBalance(bankAccDeb.getBalance() + model.getAmount());
		// ----------------------------------------------------------------
		if (model.getPurposeOfPayment() == "") {
			uplata = "Uplata pazara";
		} else {
			uplata = model.getPurposeOfPayment();
		}
		Transakcije tr = new Transakcije();
		tr.setDebtor(bankAccountServiceInterface.DTOToBankAccount(bankAccRec));
		tr.setPurposeOfPayment(uplata);
		tr.setCreditor(principal);
		tr.setDateOfReceipt(date);
		tr.setDateOfValue(date);
		tr.setDebtorAccount(model.getNumberDebtor());
		tr.setDebitModel(model.getModel());
		tr.setCallNumber(model.getCallNumber());
		tr.setUrgently(model.isMurgently());
		tr.setAmount((int) model.getAmount());
		tr.setError(null);
		tr.setStatus(true);
		tr.setCurrencies(currencies);
		tr.setDailyCondition(dailyCondition);
		tr.setPlace(place);
		tr.setTypeOfPayments(types);
		tr.setMessage(m);
//		 		bankAccounServiceInterface.insert(bankAccRec); 
//		 		bankAccounServiceInterface.save(bankAccountServiceInterface.DTOToBankAccount(bankAccRec));

		transactionServiceInterface.save(tr);

		return transactionServiceInterface.transactionToDTO(tr);

	}

	
	public TransakcijeDTO medjubankarskiPrenos(ModelDTO model, Principal principal) {

		BankAccountDTO bankAccRec = bankAccountServiceInterface.findByNumber(model.getNumberRecipient());

		if (bankAccRec.getBalance() < model.getAmount()) {
			return null;
		}
		bankAccRec.setBalance(bankAccRec.getBalance() - model.getAmount());
		UUID uuid = UUID.randomUUID();

		
		DailyAccountBalanceDTO	dailyAccountBalanceDTO  = new DailyAccountBalanceDTO();
		
		Date date = new Date(System.currentTimeMillis());
		dailyAccountBalanceDTO.setDate(date);
		dailyAccountBalanceDTO.setFreightFraffic(908-20001-18);
		dailyAccountBalanceDTO.setId(uuid);
		dailyAccountBalanceDTO.setNewState(model.getAmount());
		dailyAccountBalanceDTO.setPreviousState(0);
		dailyAccountBalanceDTO.setTurnoverInFavor(908-20001-18);
		dailyAccountBalanceDTO.setMurgently(model.isMurgently());

		
		RestTemplate restTemplate = new RestTemplate();
		Model result = restTemplate.postForObject(call, dailyAccountBalanceDTO, Model.class);

		
		
		System.err.println("stigao na mesto  "+result);

		if (result != null) {
			
			saveTransactionMetod(model);
			
		}
		
		
//		String uplata;
//		if (result != null) {
//			
//			Set<Currency> currencies = new HashSet<Currency>();
//
//			System.currentTimeMillis();
//
//			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//			Date date = new Date(System.currentTimeMillis());
//			
//			
//			DailyCondition dailyCondition = dailyConditionServiceInterface.findById(1);
//			PopulatedPlace place = populatedPlaceInterface.get(1);
//			TypesOfPayments types = typesOfPaymentsInterface.get(1);
//			Message m = messageServiceInterface.get(1);
//			
//			
//			
//			BankAccountDTO bankAccRec1 = bankAccountServiceInterface.findByNumber(model.getNumberRecipient());
//
//			Transakcije tr = new Transakcije();
//			tr.setDebtor(bankAccountServiceInterface.DTOToBankAccount(bankAccRec1));
//
//			if (model.getPurposeOfPayment() == "") {
//				uplata = "Uplata pazara";
//			} else {
//				uplata = model.getPurposeOfPayment();
//			}
//
//			tr.setPurposeOfPayment(uplata);
//			tr.setCreditor(principal.getName());
//			tr.setDateOfReceipt(date);
//			tr.setDateOfValue(date);
//			tr.setDebtorAccount(model.getNumberDebtor());
//			tr.setDebitModel(model.getModel());
//			tr.setCallNumber(model.getCallNumber());
//			tr.setUrgently(model.isMurgently());
//			tr.setAmount((int) model.getAmount());
//			tr.setError(null);
//			tr.setStatus(true);
//			tr.setCurrencies(currencies);
//			tr.setDailyCondition(dailyCondition);
//			tr.setPlace(place);
//			tr.setTypeOfPayments(types);
//
//			tr.setMessage(m);
//
////	 		bankAccounServiceInterface.save(bankAccountServiceInterface.DTOToBankAccount(bankAccRec));
//
////				bankAccountServiceInterface.insert(bankAccRec);   
//
//			transactionServiceInterface.save(tr);
//
//			return transactionServiceInterface.transactionToDTO(tr);
//		} else
		return null;

	}

//	@PostMapping(path = "/save", consumes = "application/json")
	public ResponseEntity<TransakcijeDTO> saveTransactionMetod(@RequestBody ModelDTO model) {

		System.err.println("stigao na mesto");
		String uplata;
		if (model != null) {

			Set<Currency> currencies = new HashSet<Currency>();

			System.currentTimeMillis();

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			Date date = new Date(System.currentTimeMillis());

			DailyCondition dailyCondition = dailyConditionServiceInterface.findById(1);
			PopulatedPlace place = populatedPlaceInterface.get(1);
			TypesOfPayments types = typesOfPaymentsInterface.get(1);
			Message m = messageServiceInterface.get(1);

			BankAccountDTO bankAccRec1 = bankAccountServiceInterface.findByNumber(model.getNumberRecipient());

			Transakcije tr = new Transakcije();
			tr.setDebtor(bankAccountServiceInterface.DTOToBankAccount(bankAccRec1));

			if (model.getPurposeOfPayment() == "") {
				uplata = "Uplata pazara";
			} else {
				uplata = model.getPurposeOfPayment();
			}

			tr.setPurposeOfPayment(uplata);
			tr.setCreditor(bankAccRec1.getUser().getUsername());
			tr.setDateOfReceipt(date);
			tr.setDateOfValue(date);
			tr.setDebtorAccount(model.getNumberDebtor());
			tr.setDebitModel(model.getModel());
			tr.setCallNumber(model.getCallNumber());
			tr.setUrgently(model.isMurgently());
			tr.setAmount((int) model.getAmount());
			tr.setError(null);
			tr.setStatus(true);
			tr.setCurrencies(currencies);
			tr.setDailyCondition(dailyCondition);
			tr.setPlace(place);
			tr.setTypeOfPayments(types);

			tr.setMessage(m);

//	 		bankAccounServiceInterface.save(bankAccountServiceInterface.DTOToBankAccount(bankAccRec));

//				bankAccountServiceInterface.insert(bankAccRec);   

			transactionServiceInterface.save(tr);

//			return transactionServiceInterface.transactionToDTO(tr);
		} else
			return null;

		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@GetMapping(path = "/searchTransaction/{vremePocetno}/{vremeKrajnje}")
	public ResponseEntity<InputStreamResource> getTransactionByDate(@PathVariable String vremePocetno,
			@PathVariable String vremeKrajnje, Principal principal) throws ParseException, FileNotFoundException {
System.out.println("TransactionController.getTransactionByDate()");
//		System.out.println("searchTransaction"+principal.getName());
		System.out.println("vremePocetno" + vremePocetno);
		System.out.println("vremeKrajnje" + vremeKrajnje);

		System.out.println(vremePocetno + vremeKrajnje);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date d1 = format.parse(vremePocetno);
		Date d2 = format.parse(vremeKrajnje);
		System.out.println(d1 + "s" + d2);

		List<TransakcijeDTO> transakcijeDTOs = new ArrayList<TransakcijeDTO>();
		transakcijeDTOs = transactionServiceInterface.searchTransactionBetweenDate(d1, d2);

		writeXml(transakcijeDTOs);



	        
			//-----------------------------------------------------------------------------------------------
			
			   MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "Transakcije.xml");
		 
		        File file = new File("C:\\\\lucine\\\\BankXml\\\\Transakcije.xml");

		        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

		        return ResponseEntity.ok()
		        		
		    	    

		                // Content-Disposition
		                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
		                // Content-Type
		                .contentType(mediaType)
		                // Contet-Length
		                .contentLength(file.length()) //
		                .body(resource);
			
				//-----------------------------------------------------------------------------------------------


		
		
	}

	@GetMapping(path = "/izvodPDF/{vremePocetno}/{vremeKrajnje}")
	public ResponseEntity<InputStreamResource> reportLegalToPDF(@PathVariable String vremePocetno,
			@PathVariable String vremeKrajnje, Principal principal) throws ParseException {
		System.out.println("-----------------" + principal.getName());
//		aaleksic

		UserDTO user = userServiceInterface.findByUsername(principal.getName());

		List<Transakcije> transakcije = transactionServiceInterface.findByCreditorContains(user.getUsername());
		System.out.println("size   " + transakcije.size());

		for (int i = 0; i < transakcije.size(); i++) {

			String u = transakcije.get(i).getCreditor();

			Map<String, Object> parameters = new HashMap<String, Object>();
			System.out.println(vremePocetno);
			parameters.put("datum_pocetka", new SimpleDateFormat("yyyy-MM-dd").parse(vremePocetno));
			parameters.put("datum_kraja", new SimpleDateFormat("yyyy-MM-dd").parse(vremeKrajnje));

			parameters.put("creditor", u);
			try {
				Connection con = (Connection) DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/poslovna_banka?useSSL=true&createDatabaseIfNotExist=true", "root",
						"root");
				JasperReport jasperReport = JasperCompileManager
						.compileReport(getClass().getResource("/jasper/IzvodKlijenta.jrxml").openStream());
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, con);

				// promenite putanju
				JasperExportManager.exportReportToPdfFile(jasperPrint, "C:\\lucine\\BankXml\\Izvod.pdf");
		
			//-----------------------------------------------------------------------------------------------
				
				   MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "Izvod.pdf");
			        System.out.println("fileName: " + "Izvod.pdf");
			        System.out.println("mediaType: " + mediaType);
			 
			        File file = new File("C:\\\\lucine\\\\BankXml\\\\Izvod.pdf");
			        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
			        System.out.println("mediaType: ");
			        return ResponseEntity.ok()
			        		
			    	    

			                // Content-Disposition
			                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
			                // Content-Type
			                .contentType(mediaType)
			                // Contet-Length
			                .contentLength(file.length()) //
			                .body(resource);
				
					//-----------------------------------------------------------------------------------------------

				
				
			
			
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	public void writeXml(List<TransakcijeDTO> t) {
		System.out.println("writeXml------");

		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			// add elements to Document
			Element rootElement = doc.createElement("Transactions");
			// append root element to document
			doc.appendChild(rootElement);

			// append first child element to root element
			for (TransakcijeDTO transakcijeDTO : t) {
				rootElement.appendChild(getEmployee(doc, transakcijeDTO));
			}

			// append second child
			// rootElement.appendChild(getEmployee(doc, "2", "Lisa", "35", "Manager",
			// "Female"));

			// for output to file, console
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			// for pretty print
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);

			// write to console and file
			StreamResult console = new StreamResult(System.out);
			StreamResult file = new StreamResult(new File(XML_FILE_PATH + "Transakcije.xml"));

			// write data
			transformer.transform(source, console);
			transformer.transform(source, file);
			System.out.println("DONE");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static Node getEmployee(Document doc, TransakcijeDTO t) {

		Element employee = doc.createElement("Transaction");

		// set id attribute
		employee.setAttribute("id", String.valueOf(t.getId()));

		// create name element
		Node debtor = employee.appendChild(getEmployeeElements(doc, employee, "debtor"));

		// create age element
		Node recipient = employee.appendChild(getEmployeeElements(doc, employee, "recipient"));

		// create role element
		Node payment = employee.appendChild(getEmployeeElements(doc, employee, "payment"));

		// Transaction
		Attr attr = doc.createAttribute("id");
		attr.appendChild(doc.createTextNode(String.valueOf(t.getId())));
		((Element) employee).setAttributeNode(attr);

		// Payment urgents
		Element urgent = doc.createElement("urgent");
		urgent.appendChild(doc.createTextNode(String.valueOf(t.isUrgently())));
		payment.appendChild(urgent);

		// Debtor ID

		// Recipient ID
		Attr recipientId = doc.createAttribute("id");
		recipientId.setValue(String.valueOf(t.getDebtor().getId()));
		((Element) recipient).setAttributeNode(recipientId);

		// Debtor INFO
		Attr debtor1 = doc.createAttribute("id");
		debtor1.setValue(String.valueOf(t.getDebtor().getId()));
		((Element) debtor).setAttributeNode(debtor1);
		Element firstNameDebtor = doc.createElement("firstname");
		firstNameDebtor.appendChild(doc.createTextNode(t.getDebtor().getUser().getFirstname()));
		debtor.appendChild(firstNameDebtor);
		Element lastNameDebtor = doc.createElement("lastName");
		lastNameDebtor.appendChild(doc.createTextNode(t.getDebtor().getUser().getLastname()));
		debtor.appendChild(lastNameDebtor);
		Element accountDebotor = doc.createElement("account");
		accountDebotor.appendChild(doc.createTextNode(t.getDebtor().getNumber()));
		debtor.appendChild(accountDebotor);
//       
//      // Recipient INFO
		Element account = doc.createElement("account");
		account.appendChild(doc.createTextNode(t.getDebtorAccount()));
		recipient.appendChild(account);
//    
//    //Payment INFO
////	//Reason
		Element paymentReason = doc.createElement("payment_reason");
		paymentReason.appendChild(doc.createTextNode(t.getPurposeOfPayment()));
		payment.appendChild(paymentReason);
		// code
		Element paymentCode = doc.createElement("code");
		paymentCode.appendChild(doc.createTextNode(String.valueOf(t.getCallNumber())));
		payment.appendChild(paymentCode);
		// Currency
		Element currency = doc.createElement("currency");
		currency.appendChild(doc.createTextNode("Kes"));
		payment.appendChild(currency);
//    //Amount
		Element amount = doc.createElement("amount");
		amount.appendChild(doc.createTextNode(String.valueOf(t.getAmount())));
		payment.appendChild(amount);
		// model
		Element model = doc.createElement("model");
		model.appendChild(doc.createTextNode(String.valueOf(t.getDebitModel())));
		payment.appendChild(model);
		// call number
		Element call_number = doc.createElement("call_number");
		call_number.appendChild(doc.createTextNode(String.valueOf(t.getCallNumber())));
		payment.appendChild(call_number);
		// date created
		Element date = doc.createElement("date");
		date.appendChild(doc.createTextNode(String.valueOf(t.getDateOfReceipt())));
		payment.appendChild(date);

		return employee;
	}

	private static Node getEmployeeElements(Document doc, Element element, String name) {
		Element node = doc.createElement(name);
		return node;
	}

	
	
	
	
	
	
	
	
	
	@PostMapping(value = "/importXML")
	public ResponseEntity<List<TransakcijeDTO>> importXML(@RequestParam("files") MultipartFile[] files)
			throws ParserConfigurationException, SAXException, IOException {

		
		Path pathXml=null;
		
		if (files == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		}  
		
		
		
		
		
		
//		upladDirectory
		StringBuilder fileNames = new StringBuilder();

		for (MultipartFile file : files) {

			Path fileNameAndPath = Paths.get(upladDirectory, file.getOriginalFilename());

			fileNames.append(file.getOriginalFilename());

			try {
				pathXml=fileNameAndPath;
				System.out.println("pathXml"+pathXml.toString()+file.getOriginalFilename());

				Files.write(fileNameAndPath, file.getBytes());

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		

		
		List<TransakcijeDTO> transakcijeDtoXml=new ArrayList<TransakcijeDTO>();
		
		// Get Document Builder
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		// Build Document
		Document document = builder.parse(new File(pathXml.toString()));

		// Normalize the XML Structure; It's just too important !!
		document.getDocumentElement().normalize();

		// Here comes the root node
		Element root = document.getDocumentElement();
		System.out.println(root.getNodeName());

		// Get all employees
		NodeList nList = document.getElementsByTagName("Transaction");
		System.out.println("============================");

		
	
		
		for (int temp = 0; temp < nList.getLength(); temp++) {
			TransakcijeDTO transakcijeDTO = new TransakcijeDTO();
			Node node = nList.item(temp);
			System.out.println(""); // Just a separator
			if (node.getNodeType() == Node.ELEMENT_NODE) {


			
				
				Element eElement = (Element) node;
				System.out.println("Employee id : " + eElement.getAttribute("id"));

				
				
//				if(eElement.getAttribute("id").equalsIgnoreCase("1")) {
				System.out.println("============debtor======1==========");
				
				
				NodeList nListdebtor = document.getElementsByTagName("debtor");
				Node node1 = nListdebtor.item(temp);				
				Element eElementDebtor = (Element) node1;
				
				System.out.println(eElementDebtor.getElementsByTagName("firstname").item(0).getTextContent());
				System.out.println(eElementDebtor.getElementsByTagName("account").item(0).getTextContent());

				transakcijeDTO.setCreditor(eElementDebtor.getElementsByTagName("firstname").item(0).getTextContent());
				
				
								
				BankAccount bankAccount = new BankAccount();
//				bankAccount.setNumber(eElementDebtor.getElementsByTagName("account").item(0).getTextContent());
				bankAccount.setNumber(eElementDebtor.getElementsByTagName("account").item(0).getTextContent());
				transakcijeDTO.setDebtor(bankAccount);
				
				
		
				
				
		
				
				
				

				
				
				
				System.out.println("=============recipient====1===========");
				
				NodeList nListrecipient = document.getElementsByTagName("recipient");
				Node nodeRecipient = nListrecipient.item(temp);				
				Element eElementrecipient = (Element) nodeRecipient;
				
				System.out.println(eElementrecipient.getElementsByTagName("account").item(0).getTextContent());

				transakcijeDTO.setDebtorAccount(eElementrecipient.getElementsByTagName("account").item(0).getTextContent());

			
				
				
			
				
				
			
				System.out.println("===============payment=====1========");
				
				
				NodeList nListPayment = document.getElementsByTagName("payment");
				Node nodePayment = nListPayment.item(temp);				
				Element eElementPayment = (Element) nodePayment;
				
		
				System.out.println(eElementPayment.getElementsByTagName("urgent").item(0).getTextContent());
				System.out.println(eElementPayment.getElementsByTagName("payment_reason").item(0).getTextContent());
				System.out.println(eElementPayment.getElementsByTagName("code").item(0).getTextContent());
				System.out.println(eElementPayment.getElementsByTagName("amount").item(0).getTextContent());
				System.out.println(eElementPayment.getElementsByTagName("model").item(0).getTextContent());
				System.out.println(eElementPayment.getElementsByTagName("call_number").item(0).getTextContent());
				System.out.println(eElementPayment.getElementsByTagName("date").item(0).getTextContent());
				
				transakcijeDTO.setId(Integer.parseInt(eElement.getAttribute("id")));
				transakcijeDTO.setUrgently(Boolean(eElementPayment.getElementsByTagName("urgent").item(0).getTextContent()));
				transakcijeDTO.setPurposeOfPayment(eElementPayment.getElementsByTagName("payment_reason").item(0).getTextContent());
				transakcijeDTO.setAmount(Integer.parseInt(eElementPayment.getElementsByTagName("amount").item(0).getTextContent()));
				transakcijeDTO.setDebitModel(Integer.parseInt(eElementPayment.getElementsByTagName("model").item(0).getTextContent()));
				transakcijeDTO.setCallNumber(Integer.parseInt(eElementPayment.getElementsByTagName("call_number").item(0).getTextContent()));
				
				
				transakcijeDtoXml.add(transakcijeDTO);
//				}
			}


		}
//		for( TransakcijeDTO  cer : transakcijeDtoXml) {
//	System.err.println(cer);
//		}
		return new ResponseEntity<>(transakcijeDtoXml, HttpStatus.OK);

	}

	private boolean Boolean(String textContent) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@PostMapping(value = "/deleteTransactionFromXml/{id}")
	public ResponseEntity<List<TransakcijeDTO>> deleteTransactionInXmlFile(@PathVariable String id)
			throws ParserConfigurationException, SAXException, IOException {


		
		List<TransakcijeDTO> transakcijeDtoXml=new ArrayList<TransakcijeDTO>();
		
		// Get Document Builder
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		// Build Document
		Document document = builder.parse(new File("C:\\\\lucine\\\\BankXml\\\\Transakcije.xml".toString()));

		// Normalize the XML Structure; It's just too important !!
		document.getDocumentElement().normalize();

		// Here comes the root node
		Element root = document.getDocumentElement();
		System.out.println(root.getNodeName());

		// Get all employees
		NodeList nList = document.getElementsByTagName("Transaction");
		System.out.println("============================");

		
	
		
		for (int temp = 0; temp < nList.getLength(); temp++) {
			TransakcijeDTO transakcijeDTO = new TransakcijeDTO();
			Node node = nList.item(temp);
			System.out.println(""); // Just a separator
			if (node.getNodeType() == Node.ELEMENT_NODE) {


			
				
				Element eElement = (Element) node;
				System.out.println("Employee id : " + eElement.getAttribute("id"));

				
				
				if(!eElement.getAttribute("id").equals(id)) {
				System.out.println("============debtor======1==========");
				
				
				NodeList nListdebtor = document.getElementsByTagName("debtor");
				Node node1 = nListdebtor.item(temp);				
				Element eElementDebtor = (Element) node1;
				
				System.out.println(eElementDebtor.getElementsByTagName("firstname").item(0).getTextContent());
				System.out.println(eElementDebtor.getElementsByTagName("account").item(0).getTextContent());

				transakcijeDTO.setCreditor(eElementDebtor.getElementsByTagName("firstname").item(0).getTextContent());
				
				
								
				BankAccount bankAccount = new BankAccount();
//				bankAccount.setNumber(eElementDebtor.getElementsByTagName("account").item(0).getTextContent());
				bankAccount.setNumber(eElementDebtor.getElementsByTagName("account").item(0).getTextContent());
				transakcijeDTO.setDebtor(bankAccount);

				
				System.out.println("=============recipient====1===========");
				
				NodeList nListrecipient = document.getElementsByTagName("recipient");
				Node nodeRecipient = nListrecipient.item(temp);				
				Element eElementrecipient = (Element) nodeRecipient;
				System.out.println(eElementrecipient.getElementsByTagName("account").item(0).getTextContent());
				transakcijeDTO.setDebtorAccount(eElementrecipient.getElementsByTagName("account").item(0).getTextContent());


				System.out.println("===============payment=====1========");
				
				NodeList nListPayment = document.getElementsByTagName("payment");
				Node nodePayment = nListPayment.item(temp);				
				Element eElementPayment = (Element) nodePayment;
				
		
				System.out.println(eElementPayment.getElementsByTagName("urgent").item(0).getTextContent());
				System.out.println(eElementPayment.getElementsByTagName("payment_reason").item(0).getTextContent());
				System.out.println(eElementPayment.getElementsByTagName("code").item(0).getTextContent());
				System.out.println(eElementPayment.getElementsByTagName("amount").item(0).getTextContent());
				System.out.println(eElementPayment.getElementsByTagName("model").item(0).getTextContent());
				System.out.println(eElementPayment.getElementsByTagName("call_number").item(0).getTextContent());
				System.out.println(eElementPayment.getElementsByTagName("date").item(0).getTextContent());
				
				transakcijeDTO.setId(Integer.parseInt(eElement.getAttribute("id")));
				transakcijeDTO.setUrgently(Boolean(eElementPayment.getElementsByTagName("urgent").item(0).getTextContent()));
				transakcijeDTO.setPurposeOfPayment(eElementPayment.getElementsByTagName("payment_reason").item(0).getTextContent());
				transakcijeDTO.setAmount(Integer.parseInt(eElementPayment.getElementsByTagName("amount").item(0).getTextContent()));
				transakcijeDTO.setDebitModel(Integer.parseInt(eElementPayment.getElementsByTagName("model").item(0).getTextContent()));
				transakcijeDTO.setCallNumber(Integer.parseInt(eElementPayment.getElementsByTagName("call_number").item(0).getTextContent()));
				

				transakcijeDTO.setDebtor(bankAccountRepository.findByNumber(transakcijeDTO.getDebtor().getNumber()));
				
				transakcijeDTO.getDebtor().setUser(userRepository.findByFirstname(transakcijeDTO.getDebtor().getUser().getFirstname()));

				transakcijeDtoXml.add(transakcijeDTO);
				}
			}

			writeXml(transakcijeDtoXml);
		}
		
		
		

		return new ResponseEntity<>(transakcijeDtoXml, HttpStatus.OK);

	}
}
