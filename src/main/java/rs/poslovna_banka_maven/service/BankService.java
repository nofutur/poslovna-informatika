package rs.poslovna_banka_maven.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.poslovna_banka_maven.entity.*;
import rs.poslovna_banka_maven.dto.BankDTO;
import rs.poslovna_banka_maven.dto.UserDTO;
import rs.poslovna_banka_maven.repository.BankRepository;
import rs.poslovna_banka_maven.repository.UserRepository;

@Service
public class BankService implements BankServiceInterface {

	@Autowired
	BankRepository bankRepository;
	@Autowired
	UserRepository userRepository;

	@Override
	public BankDTO findByUsername(String username) {
		
		
		System.err.println(username);
		Bank bank = bankRepository.findByUsername("pbanka");
		System.out.println("bank"+bank.getUsername());
		BankDTO bankDTO= new BankDTO();
//		bankDTO.setAccountingBill(bank.getAccountingBill());
		bankDTO.setActivated(bank.isActivated());
		bankDTO.setAddress(bank.getAddress());
		bankDTO.setBalance(bank.getBalance());
		bankDTO.setCode(bank.getCode());
		bankDTO.setFax(bank.getFax());
		bankDTO.setId(bank.getId());
		bankDTO.setUsername(bank.getUsername());
		bankDTO.setName(bank.getName());
		bankDTO.setWeb(bank.getWeb());
		bankDTO.setPhone(bank.getPhone());
		bankDTO.setTIN(bank.getTIN());
		
		
		return bankDTO;

	}

	@Override
	public List<BankDTO> findAll() {
		List<BankDTO> banks = new ArrayList<BankDTO>();
		List<Bank> b = bankRepository.findAll();
		
		for(Bank ba : b) {
			BankDTO bank = new BankDTO(ba);
			banks.add(bank);
		}
		return banks;
	}

	
//	@Override
//	public List<>  List<UserDTO> findByUsername(String username,boolean legalEntity) {
//		System.err.println(username);
//		Bank bank = bankRepository.findByUsername(username);
//		System.err.println(bank);
//
//		System.out.println("bank"+bank.getId());
//		
//		List<User> users = userRepository.findByBankAndLegalEntity(bank.getId(), legalEntity);
//		
//		List<UserDTO> userDTOs = new ArrayList<UserDTO>();
//		
//		for (User itUser : users) {
//			userDTOs.add(new UserDTO(itUser));
//		}
//
//		return userDTOs;
//		return null;
//
//	}
	

}
