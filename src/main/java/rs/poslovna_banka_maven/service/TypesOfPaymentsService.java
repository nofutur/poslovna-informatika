package rs.poslovna_banka_maven.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.poslovna_banka_maven.entity.TypesOfPayments;
import rs.poslovna_banka_maven.repository.TypesOfPaymentsRepository;

@Service
public class TypesOfPaymentsService implements TypesOfPaymentsInterface{
	@Autowired
	private TypesOfPaymentsRepository typesOfPaymentsRepo;
	
	@Override
    public List<TypesOfPayments> getAll() {
		return typesOfPaymentsRepo.findAll();
    }
	
	@Override
    public TypesOfPayments get(Integer id) {
    	Optional<TypesOfPayments> tOfP = typesOfPaymentsRepo.findById(id);
        return tOfP.get();
    }
	
	@Override
    public TypesOfPayments insert(TypesOfPayments tOfP) {
    	return typesOfPaymentsRepo.save(tOfP);
    }
	
	@Override
    public TypesOfPayments edit(TypesOfPayments tOfP) {
        return typesOfPaymentsRepo.save(tOfP);
    }
	
	@Override
    public void delete(Integer id) {
		typesOfPaymentsRepo.deleteById(id);
    }
}
