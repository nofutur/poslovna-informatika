package rs.poslovna_banka_maven.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import rs.poslovna_banka_maven.dto.DailyConditionDTO;

@Entity
@Table(name="Daily_Conditions")
public class DailyCondition implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "daily_condition_id",unique = true,nullable = false)
	private int id;
	
	@Column(name = "previous_state",nullable = false,unique = true)
	private double previousState;
	
	@Column(name = "newState",nullable = false,unique = true)
	private double newState;
	
	//@OneToOne(mappedBy = "conditions" )
	@OneToMany(cascade= {ALL}, fetch=LAZY, mappedBy="dailyCondition")
	private Set<Transakcije> transakcije;
	
	@ManyToOne
	@JoinColumn(name="bank_account_id", referencedColumnName="bank_account_id", nullable=false)
	private BankAccount bankAccount;

	public DailyCondition() {}

	public DailyCondition(DailyConditionDTO d) {
		super();
		this.id = d.getId();
		this.previousState = d.getPreviousState();
		this.newState = d.getNewState();
		this.bankAccount=d.getBankAccount();

	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPreviousState() {
		return previousState;
	}

	public void setPreviousState(double previousState) {
		this.previousState = previousState;
	}

	public double getNewState() {
		return newState;
	}

	public void setNewState(double newState) {
		this.newState = newState;
	}

	

	public Set<Transakcije> getTransakcije() {
		return transakcije;
	}

	public void setTransakcije(Set<Transakcije> transakcije) {
		this.transakcije = transakcije;
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}
	
}
