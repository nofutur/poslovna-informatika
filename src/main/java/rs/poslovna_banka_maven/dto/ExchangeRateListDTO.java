package rs.poslovna_banka_maven.dto;

import java.util.Date;
import rs.poslovna_banka_maven.entity.Bank;
import rs.poslovna_banka_maven.entity.ExchangeRateList;
public class ExchangeRateListDTO {

	private int id;
	private Date date;
	private int numberOfExchangeRateList;
	private Date appliedBy;
	private BankDTO bank;
	
	public ExchangeRateListDTO() {
		
	}
	
	public ExchangeRateListDTO(ExchangeRateList erl) {
		this.id = erl.getId();
		this.date = erl.getDate();
		this.numberOfExchangeRateList = erl.getNumberOfExchangeRateList();
		this.appliedBy = erl.getAppliedBy();

	}
	
	public ExchangeRateListDTO(Date date, int numberOfExchangeRateList, Date appliedBy, BankDTO bank) {
		super();
		this.date = date;
		this.numberOfExchangeRateList = numberOfExchangeRateList;
		this.appliedBy = appliedBy;
		this.bank = bank;
	}
	

	public ExchangeRateListDTO(int id, Date date, int numberOfExchangeRateList, Date appliedBy, BankDTO bank) {
		super();
		this.id = id;
		this.date = date;
		this.numberOfExchangeRateList = numberOfExchangeRateList;
		this.appliedBy = appliedBy;
		this.bank = bank;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getNumberOfExchangeRateList() {
		return numberOfExchangeRateList;
	}

	public void setNumberOfExchangeRateList(int numberOfExchangeRateList) {
		this.numberOfExchangeRateList = numberOfExchangeRateList;
	}

	public Date getAppliedBy() {
		return appliedBy;
	}

	public void setAppliedBy(Date appliedBy) {
		this.appliedBy = appliedBy;
	}

	public BankDTO getBank() {
		return bank;
	}

	public void setBank(BankDTO bank) {
		this.bank = bank;
	}
}
