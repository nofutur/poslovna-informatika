package rs.poslovna_banka_maven.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.poslovna_banka_maven.dto.BankAccountDTO;
import rs.poslovna_banka_maven.dto.CountryDTO;
import rs.poslovna_banka_maven.dto.CurrencyDTO;
import rs.poslovna_banka_maven.dto.TransakcijeDTO;

@Entity
@Table(name="currencies")
public class Currency implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "currency_id",unique = true,nullable = false)
	private int id;
	
	@Column(name = "code_of_currency",unique = true,nullable = false, length = 15)
	private String codeOfCurrency;
	
	@Column(name = "name",nullable = false, length = 30)
	private String name;

	@OneToMany( fetch=FetchType.LAZY,cascade = CascadeType.ALL, mappedBy="currencies")
	@JsonIgnore
	private Set<Transakcije> transactions;
	
	@OneToMany(cascade= {ALL}, fetch=LAZY, mappedBy="currency")
	@JsonIgnore
	private Set<BankAccount> bankAccounts = new HashSet<BankAccount>();
	
	@OneToMany( fetch=FetchType.LAZY ,cascade = CascadeType.ALL, mappedBy="currency")
	@JsonIgnore
	private Set<Country> countries;
	
	public Currency() {
	}
	
	public void addBankAccount(BankAccount bankAccount) {
		if (bankAccount.getCurrency() != null) {
			bankAccount.getCurrency().getBankAccounts().remove(bankAccount);
		}
		bankAccount.setCurrency(this);
		getBankAccounts().add(bankAccount);
	}
	
	public void removeBankAccount(BankAccount bankAccount) {
		bankAccount.setCurrency(null);
		getBankAccounts().remove(bankAccount);
	}
	

	public Currency(CurrencyDTO c) {
		super();
		this.id = c.getId();
		this.codeOfCurrency = c.getCodeOfCurrency();
		this.name = c.getName(); 
		
		if(c.getCountries() != null) {
			for (CountryDTO co :c.getCountries() ) {
				this.countries.add(new Country(co));
			}
		}

	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodeOfCurrency() {
		return codeOfCurrency;
	}

	public void setCodeOfCurrency(String codeOfCurrency) {
		this.codeOfCurrency = codeOfCurrency;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Transakcije> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<Transakcije> transactions) {
		this.transactions = transactions;
	}

	public Set<BankAccount> getBankAccounts() {
		return bankAccounts;
	}

	public void setBankAccounts(Set<BankAccount> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

	public Set<Country> getCountries() {
		return countries;
	}

	public void setCountries(Set<Country> countries) {
		this.countries = countries;
	}
	
	
}
