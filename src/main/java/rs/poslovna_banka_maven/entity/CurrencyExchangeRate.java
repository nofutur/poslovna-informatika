package rs.poslovna_banka_maven.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.poslovna_banka_maven.dto.CurrencyExchangeRateDTO;

@Entity
@Table(name = "currency_exchange_rate")
public class CurrencyExchangeRate implements Serializable {
	
	public enum Error{
		error_with_server
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="Ordinal_number")
	private int ordinalNumber;
	
	@Column(name = "Buying")
	private double buying;
	
	@Column(name = "Middle")
	private double middle;

	@Column(name = "Selling")
	private double selling;
	
	@ManyToOne
	@JoinColumn(name="currency_id", referencedColumnName="currency_id", nullable = false)
	@JsonIgnore
	private Currency currency;
	
	@ManyToOne
	@JoinColumn(name="exchangeRateList_id", referencedColumnName="exchangeRateList_id", nullable = false)
	@JsonIgnore
	private ExchangeRateList exchangeRateList;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOrdinalNumber() {
		return ordinalNumber;
	}

	public void setOrdinalNumber(int ordinalNumber) {
		this.ordinalNumber = ordinalNumber;
	}

	public double getBuying() {
		return buying;
	}

	public void setBuying(double buying) {
		this.buying = buying;
	}

	public double getMiddle() {
		return middle;
	}

	public void setMiddle(double middle) {
		this.middle = middle;
	}

	public double getSelling() {
		return selling;
	}

	public void setSelling(double selling) {
		this.selling = selling;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public ExchangeRateList getExchangeRateList() {
		return exchangeRateList;
	}

	public void setExchangeRateList(ExchangeRateList exchangeRateList) {
		this.exchangeRateList = exchangeRateList;
	}

	public CurrencyExchangeRate(CurrencyExchangeRateDTO cer) {
		this.id = cer.getId();
		this.ordinalNumber = cer.getOrdinalNumber();
		this.buying = cer.getBuying();
		this.middle = cer.getMiddle();
		this.selling = cer.getSelling();
		this.currency = cer.getCurrency();
		this.exchangeRateList = cer.getExchangeRateList();
	}

	public CurrencyExchangeRate() {
		
	}
}
