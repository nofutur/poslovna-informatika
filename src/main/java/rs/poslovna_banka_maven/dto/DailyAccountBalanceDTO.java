package rs.poslovna_banka_maven.dto;

import java.util.Date;
import java.util.UUID;

import rs.poslovna_banka_maven.entity.BankAccount;

public class DailyAccountBalanceDTO {

	private UUID id;

	private Date date;
	
	private double previousState;

	private double turnoverInFavor;
	
	private double freightFraffic;

	private double newState;

	private boolean murgently;
	
	public DailyAccountBalanceDTO(UUID id, Date date, double previousState, double turnoverInFavor,
			double freightFraffic, double newState, boolean murgently) {
		super();
		this.id = id;
		this.date = date;
		this.previousState = previousState;
		this.turnoverInFavor = turnoverInFavor;
		this.freightFraffic = freightFraffic;
		this.newState = newState;
		this.murgently = murgently;
	}

	public boolean isMurgently() {
		return murgently;
	}

	public void setMurgently(boolean murgently) {
		this.murgently = murgently;
	}

	public DailyAccountBalanceDTO() {
		super();
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getPreviousState() {
		return previousState;
	}

	public void setPreviousState(double previousState) {
		this.previousState = previousState;
	}

	public double getTurnoverInFavor() {
		return turnoverInFavor;
	}

	public void setTurnoverInFavor(double turnoverInFavor) {
		this.turnoverInFavor = turnoverInFavor;
	}

	public double getFreightFraffic() {
		return freightFraffic;
	}

	public void setFreightFraffic(double freightFraffic) {
		this.freightFraffic = freightFraffic;
	}

	public double getNewState() {
		return newState;
	}

	public void setNewState(double newState) {
		this.newState = newState;
	}

	public DailyAccountBalanceDTO(UUID id, Date date, double previousState, double turnoverInFavor,
			double freightFraffic, double newState) {
		super();
		this.id = id;
		this.date = date;
		this.previousState = previousState;
		this.turnoverInFavor = turnoverInFavor;
		this.freightFraffic = freightFraffic;
		this.newState = newState;
	}

	
	
	
}
