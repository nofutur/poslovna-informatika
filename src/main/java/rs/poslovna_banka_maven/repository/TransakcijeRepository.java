package rs.poslovna_banka_maven.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import rs.poslovna_banka_maven.dto.TransakcijeDTO;
import rs.poslovna_banka_maven.entity.Transakcije;


@Repository
public interface TransakcijeRepository extends JpaRepository<Transakcije, String> {
	public List<Transakcije> findAllByDateOfReceiptGreaterThanEqualAndDateOfReceiptLessThanEqual(Date d1,Date d2);
	public List<Transakcije> findByCreditorContains(String d);
	
	
	
																	

	@Query("select t FROM Transakcije t where t.dateOfReceipt > :date1 and   t.dateOfReceipt < :date2 and creditor=:name  ")    
	List<Transakcije> getAll(Date date1,Date date2,String name);
	
	
	@Query("select t FROM Transakcije t where t.debtor.number in :brojDuznikaText and creditor=:name  ")    
	List<Transakcije> findByDebitor(String name,String brojDuznikaText);
	
	@Query("select t FROM Transakcije t where t.debtorAccount in :brojPrimaocaText and creditor=:name  ")    
	List<Transakcije> findByRecipient(String name,String brojPrimaocaText);
}

