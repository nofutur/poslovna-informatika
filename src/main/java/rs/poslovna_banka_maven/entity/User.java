package rs.poslovna_banka_maven.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.poslovna_banka_maven.dto.BankAccountDTO;
import rs.poslovna_banka_maven.dto.UserDTO;

@Entity
@Table(name = "users")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5448125200461299152L;




	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "user_id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "user_username", unique = true, nullable = false, length = 20)
	private String username;
	
	@Column(name = "user_password", unique = false, nullable = false, length = 200)
	private String password;
	
	@Column(name = "user_firstname", unique = false, nullable = false, length = 100)
	private String firstname;

	@Column(name = "user_lastname", unique = false, nullable = false, length = 100)
	private String lastname;

	@Column(name = "user_activated", unique = false, nullable = false)
	@ColumnDefault("1")
	private boolean activated;

	@Column(name = "user_address ")
	private String address;

	@Column(name = "user_id_number")
	private String identificationNumber;

	@Column(name = "user_phone_number")
	private String phoneNumber;

	@Column(name = "user_legal_entity")
	@ColumnDefault("0")
	private Boolean legalEntity;


	@OneToMany(cascade= {ALL}, fetch=LAZY, mappedBy="user" )
	@JsonIgnore
	private Set<BankAccount> bankAccounts = new HashSet<BankAccount>();
	
	@ManyToMany
	@JoinTable(name = "USER_AUTHORITY", joinColumns = {
			@JoinColumn(name = "user_id", referencedColumnName = "user_id") }, inverseJoinColumns = {
					@JoinColumn(name = "AUTHORITY_NAME", referencedColumnName = "NAME") })
	private Set<Authority> authorities = new HashSet<>();
	

	public User() {
	}





	public User(Integer id, String username, String firstname, String lastname, boolean activated, String address,
			String identificationNumber, String phoneNumber, Boolean legalEntity, Set<BankAccount> bankAccounts,
			Set<Authority> authorities) {
		super();
		this.id = id;
		this.username = username;
		this.firstname = firstname;
		this.lastname = lastname;
		this.activated = activated;
		this.address = address;
		this.identificationNumber = identificationNumber;
		this.phoneNumber = phoneNumber;
		this.legalEntity = legalEntity;
		this.bankAccounts = bankAccounts;
		this.authorities = authorities;
	}

	public User(UserDTO u) {
		super();
		this.id = u.getId();
		this.username = u.getUsername();
		this.firstname = u.getFirstname();
		this.lastname = u.getLastname();
		this.activated = true;
		this.address = u.getAddress();
		this.identificationNumber = u.getIdentificationNumber();
		this.phoneNumber = u.getPhoneNumber();
		this.legalEntity = u.isLegalEntity();
		Set<BankAccount> ba = new HashSet<BankAccount>();
		for(BankAccountDTO b : u.getBankAccounts()) {
			ba.add(new BankAccount(b));
		}
		this.bankAccounts = ba;
		Set<Authority> authorities = new HashSet<Authority>();
		this.authorities = authorities;
	}




	public void addBankAccount(BankAccount bankAccount) {
		if (bankAccount.getUser() != null) {
			bankAccount.getUser().getBankAccounts().remove(bankAccount);
		}
		bankAccount.setUser(this);
		getBankAccounts().add(bankAccount);
	}
	
	public void removeBankAccount(BankAccount bankAccount) {
		bankAccount.setUser(null);
		getBankAccounts().remove(bankAccount);
	}
	

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Boolean getLegalEntity() {
		return legalEntity;
	}

	public void setLegalEntity(Boolean legalEntity) {
		this.legalEntity = legalEntity;
	}

	
	public Set<BankAccount> getBankAccounts() {
		return bankAccounts;
	}

	public void setBankAccounts(Set<BankAccount> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}

	public Set<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", firstname=" + firstname
				+ ", lastname=" + lastname + ", activated=" + activated + ", address=" + address
				+ ", identificationNumber=" + identificationNumber + ", phoneNumber=" + phoneNumber + ", legalEntity="
				+ legalEntity + "]";
	}

}
