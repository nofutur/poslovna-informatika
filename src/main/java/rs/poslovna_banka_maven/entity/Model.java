package rs.poslovna_banka_maven.entity;

public class Model {
	private double amount;
	private String code;
	private String numberDebtor;
	private String numberRecipient; 
	private String purposeOfPayment;
	private int callNumber;
	private int model;
	private boolean murgently;
	

	
	

	public String getPurposeOfPayment() {
		return purposeOfPayment;
	}



	public void setPurposeOfPayment(String purposeOfPayment) {
		this.purposeOfPayment = purposeOfPayment;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public Model(String numberRecipient, String numberDebtor, double amount, int callNumber, int model,
			boolean murgently, String purposeOfPayment, String code) {
		super();
		this.numberRecipient = numberRecipient;
		this.numberDebtor = numberDebtor;
		this.amount = amount;
		this.callNumber = callNumber;
		this.model = model;
		this.murgently = murgently;
		this.purposeOfPayment = purposeOfPayment;
		this.code = code;
	}



	public Model() {
		super();
	}



	public boolean isMurgently() {
		return murgently;
	}

	public void setMurgently(boolean murgently) {
		this.murgently = murgently;
	}

	public String getNumberRecipient() {
		return numberRecipient;
	}

	public void setNumberRecipient(String numberRecipient) {
		this.numberRecipient = numberRecipient;
	}

	public String getNumberDebtor() {
		return numberDebtor;
	}

	public void setNumberDebtor(String numberDebtor) {
		this.numberDebtor = numberDebtor;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getCallNumber() {
		return callNumber;
	}

	public void setCallNumber(int callNumber) {
		this.callNumber = callNumber;
	}

	public int getModel() {
		return model;
	}

	public void setModel(int model) {
		this.model = model;
	}
	
	
	
}

