package rs.poslovna_banka_maven.entity;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import rs.poslovna_banka_maven.dto.CountryDTO;
import rs.poslovna_banka_maven.dto.PopulatedPlacesDTO;

@Entity
@Table(name="Country")
public class Country implements Serializable{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private int id;
	
	@Column(name = "code", nullable = false)
	private String code;
	
	@Column(name = "name",  nullable = false)
	private String name;
	
	@ManyToOne
	@JoinColumn(name="currency_id", referencedColumnName="currency_id", nullable=false)
	private Currency currency;
	
	@OneToMany( fetch=FetchType.LAZY ,cascade = CascadeType.ALL, mappedBy="country")
	@JsonIgnore
	private Set<PopulatedPlace> places;
	
	public Country() {
		super();
	}

	public Country(CountryDTO c) {
		this.id = c.getId();
		this.code = c.getCode();
		this.name = c.getName();
		this.currency=c.getCurrency();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Set<PopulatedPlace> getPlaces() {
		return places;
	}

	public void setPlaces(Set<PopulatedPlace> places) {
		this.places = places;
	}
	
}