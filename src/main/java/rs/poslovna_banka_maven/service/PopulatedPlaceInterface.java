package rs.poslovna_banka_maven.service;

import java.util.List;

import rs.poslovna_banka_maven.entity.PopulatedPlace;

public interface PopulatedPlaceInterface {
	List<PopulatedPlace> getAll();
	PopulatedPlace get(Integer id);
	PopulatedPlace insert(PopulatedPlace populatedPlace);
	PopulatedPlace edit(PopulatedPlace populatedPlace);
	void delete(Integer id);
}

