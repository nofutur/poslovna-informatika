package rs.poslovna_banka_maven.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import rs.poslovna_banka_maven.entity.BankAccount;
import rs.poslovna_banka_maven.entity.Currency;
import rs.poslovna_banka_maven.entity.DailyCondition;
import rs.poslovna_banka_maven.entity.Message;
import rs.poslovna_banka_maven.entity.PopulatedPlace;
import rs.poslovna_banka_maven.entity.Transakcije;
import rs.poslovna_banka_maven.entity.TypesOfPayments;
import rs.poslovna_banka_maven.entity.Transakcije.Error;

public class TransakcijeDTO {

	private int id;
	
	private BankAccount debtor;

	private String purposeOfPayment;

	private String creditor;

	private Date dateOfReceipt;

	private Date dateOfValue;

	private String debtorAccount;

	private int debitModel;

	private int callNumber;
	
	private boolean urgently;

	private int amount;

	private Error error;

	private boolean status;
	
	private ArrayList<CurrencyDTO> currencies;
	
//	private DailyCondition dailyCondition;
//	
//	private Message message;
//	
//	private PopulatedPlace place;
//	
//	private TypesOfPayments typeOfPayments;

	public TransakcijeDTO() {
		super();
	}


	public TransakcijeDTO(Transakcije t) {
		super();
		this.id = t.getId();
		this.debtor = t.getDebtor();
		this.purposeOfPayment = t.getPurposeOfPayment();
		this.creditor = t.getCreditor();
		this.dateOfReceipt = t.getDateOfReceipt();
		this.dateOfValue = t.getDateOfValue();
		this.debtorAccount = t.getDebtorAccount();
		this.debitModel = t.getDebitModel();
		this.callNumber = t.getCallNumber();
		this.urgently = t.isUrgently();
		this.amount = t.getAmount();
		this.error = t.getError();
		this.status = t.isStatus();
		ArrayList<CurrencyDTO>sub=new ArrayList<CurrencyDTO>(); 
		if(t.getCurrencies() != null) {
			for (Currency c : t.getCurrencies()) {
				sub.add(new CurrencyDTO(c));
			}
		}
		this.currencies=sub;
//		this.dailyCondition=t.getDailyCondition();
//		this.message=t.getMessage();
//		this.place=t.getPlace();
//		this.typeOfPayments=t.getTypeOfPayments();
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public BankAccount getDebtor() {
		return debtor;
	}

	public void setDebtor(BankAccount debtor) {
		this.debtor = debtor;
	}

	public String getPurposeOfPayment() {
		return purposeOfPayment;
	}

	public void setPurposeOfPayment(String purposeOfPayment) {
		this.purposeOfPayment = purposeOfPayment;
	}

	public String getCreditor() {
		return creditor;
	}

	public void setCreditor(String creditor) {
		this.creditor = creditor;
	}

	public Date getDateOfReceipt() {
		return dateOfReceipt;
	}

	public void setDateOfReceipt(Date dateOfReceipt) {
		this.dateOfReceipt = dateOfReceipt;
	}

	public Date getDateOfValue() {
		return dateOfValue;
	}

	public void setDateOfValue(Date dateOfValue) {
		this.dateOfValue = dateOfValue;
	}

	public String getDebtorAccount() {
		return debtorAccount;
	}

	public void setDebtorAccount(String debtorAccount) {
		this.debtorAccount = debtorAccount;
	}

	public int getDebitModel() {
		return debitModel;
	}

	public void setDebitModel(int debitModel) {
		this.debitModel = debitModel;
	}

	public int getCallNumber() {
		return callNumber;
	}

	public void setCallNumber(int callNumber) {
		this.callNumber = callNumber;
	}

	public boolean isUrgently() {
		return urgently;
	}

	public void setUrgently(boolean urgently) {
		this.urgently = urgently;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}


	public ArrayList<CurrencyDTO> getCurrencies() {
		return currencies;
	}


	public void setCurrencies(ArrayList<CurrencyDTO> currencies) {
		this.currencies = currencies;
	}


	

//	public DailyCondition getDailyCondition() {
//		return dailyCondition;
//	}
//
//
//	public void setDailyCondition(DailyCondition dailyCondition) {
//		this.dailyCondition = dailyCondition;
//	}
//
//
//	public Message getMessage() {
//		return message;
//	}
//
//
//	public void setMessage(Message message) {
//		this.message = message;
//	}
//
//
//	public TypesOfPayments getTypeOfPayments() {
//		return typeOfPayments;
//	}
//
//
//	public void setTypeOfPayments(TypesOfPayments typeOfPayments) {
//		this.typeOfPayments = typeOfPayments;
//	}
//
//
//	public PopulatedPlace getPlace() {
//		return place;
//	}
//
//
//	public void setPlace(PopulatedPlace place) {
//		this.place = place;
//	}


	


	
}
