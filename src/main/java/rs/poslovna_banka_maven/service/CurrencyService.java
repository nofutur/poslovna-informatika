package rs.poslovna_banka_maven.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.poslovna_banka_maven.entity.Currency;
import rs.poslovna_banka_maven.repository.CurrencyRepository;

@Service
public class CurrencyService implements CurrencyServiceInterface{

	
	@Autowired
	CurrencyRepository cr;
	
	
	@Override
	public Currency findByCodeOfCurrency(String codeOfCurrency) {
		return cr.findByCodeOfCurrency(codeOfCurrency);
	}
	
	

}
