package rs.poslovna_banka_maven.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.poslovna_banka_maven.entity.CurrencyExchangeRate;
import rs.poslovna_banka_maven.repository.CurrencyExchangeRateRepository;

@Service
public class CurrencyExchangeRateService implements CurrencyExchangeRateServiceInterface {

	@Autowired
	private CurrencyExchangeRateRepository currencyExchangeRateRepository;
	
	@Override
    public List<CurrencyExchangeRate> getAll() {
		return currencyExchangeRateRepository.findAll();
    }
	
	@Override
    public CurrencyExchangeRate get(Integer id) {
    	Optional<CurrencyExchangeRate> currencyExchangeRate = currencyExchangeRateRepository.findById(id);
        return currencyExchangeRate.get();
    }
	
	@Override
    public CurrencyExchangeRate insert(CurrencyExchangeRate currencyExchangeRate) {
    	return currencyExchangeRateRepository.save(currencyExchangeRate);
    }
	
	@Override
    public CurrencyExchangeRate edit(CurrencyExchangeRate currencyExchangeRate) {
        return currencyExchangeRateRepository.save(currencyExchangeRate);
    }
	
	@Override
    public void delete(Integer id) {
		currencyExchangeRateRepository.deleteById(id);
    }
}
