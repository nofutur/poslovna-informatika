package rs.poslovna_banka_maven.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.poslovna_banka_maven.dto.TransakcijeDTO;
import rs.poslovna_banka_maven.entity.Transakcije;
import rs.poslovna_banka_maven.repository.TransakcijeRepository;



@Service
public class TransactionService implements TransactionServiceInterface{

	
	
	
	@Autowired
	TransakcijeRepository transakcijeRepository;

	
	
	@Override
	public List<TransakcijeDTO> getAll(Date date1,Date date2,String name) {
		
		
		
		List<Transakcije> transactons=transakcijeRepository.getAll(date1, date2, name);
		
		System.out.println("TransactionService.getAll()  "+transactons.size());
		List<TransakcijeDTO> transactonsDto=new ArrayList<TransakcijeDTO>();
		
		
		for (Transakcije transacton : transactons) {
			TransakcijeDTO TransactionDTO=new TransakcijeDTO();
			TransactionDTO.setAmount(transacton.getAmount());
			TransactionDTO.setCallNumber(transacton.getCallNumber());
			TransactionDTO.setCreditor(transacton.getCreditor());
			TransactionDTO.setDateOfReceipt(transacton.getDateOfReceipt());
			TransactionDTO.setDateOfValue(transacton.getDateOfValue());
			TransactionDTO.setDebitModel(transacton.getDebitModel());
			TransactionDTO.setDebtor(transacton.getDebtor());
			TransactionDTO.setDebtorAccount(transacton.getDebtorAccount());
			TransactionDTO.setError(transacton.getError());
			TransactionDTO.setId(transacton.getId());
			TransactionDTO.setPurposeOfPayment(transacton.getPurposeOfPayment());
			transactonsDto.add(TransactionDTO);
		}
		System.out.println("TransactionService.getAll()--  "+transactonsDto.size());

		
		return transactonsDto;
	}




	@Override
	public void save(Transakcije transakcije) {
		System.err.println("-------snimanje------------- ");

//		Transakcije transakcije=new Transakcije();
//		transakcije.setAmount(transakcijeDTO.getAmount());
//		transakcije.setCallNumber(transakcijeDTO.getCallNumber());
//		transakcije.setCreditor(transakcijeDTO.getCreditor());
//		transakcije.setDateOfReceipt(transakcijeDTO.getDateOfReceipt());
//		transakcije.setDateOfValue(transakcijeDTO.getDateOfValue());
//		transakcije.setDebitModel(transakcijeDTO.getDebitModel());
//		transakcije.setDebtor(transakcijeDTO.getDebtor());
//		transakcije.setDebtorAccount(transakcijeDTO.getDebtorAccount());
//		transakcije.setError(transakcijeDTO.getError());
//		transakcije.setId(transakcijeDTO.getId());
//		transakcije.setPurposeOfPayment(transakcijeDTO.getPurposeOfPayment());
		
		transakcijeRepository.save(transakcije);
	}




	@Override
	public List<TransakcijeDTO> searchTransactionBetweenDate(Date d1,Date d2) {
		
		
		List<Transakcije> transactons=transakcijeRepository.findAllByDateOfReceiptGreaterThanEqualAndDateOfReceiptLessThanEqual(d1, d2);
		List<TransakcijeDTO> transactonsDto=new ArrayList<TransakcijeDTO>();

		
		for (Transakcije transacton : transactons) {
			TransakcijeDTO TransactionDTO=new TransakcijeDTO();
			TransactionDTO.setAmount(transacton.getAmount());
			TransactionDTO.setCallNumber(transacton.getCallNumber());
			TransactionDTO.setCreditor(transacton.getCreditor());
			TransactionDTO.setDateOfReceipt(transacton.getDateOfReceipt());
			TransactionDTO.setDateOfValue(transacton.getDateOfValue());
			TransactionDTO.setDebitModel(transacton.getDebitModel());
			TransactionDTO.setDebtor(transacton.getDebtor());
			TransactionDTO.setDebtorAccount(transacton.getDebtorAccount());
			TransactionDTO.setError(transacton.getError());
			TransactionDTO.setId(transacton.getId());
			TransactionDTO.setPurposeOfPayment(transacton.getPurposeOfPayment());
			transactonsDto.add(TransactionDTO);
		}
		
		
		return transactonsDto;
	}




	@Override
	public List<Transakcije> findByCreditorContains(String a) {
		// TODO Auto-generated method stub
		return 	transakcijeRepository.findByCreditorContains(a);
	}
	
	@Override
	public TransakcijeDTO transactionToDTO (Transakcije transacton) {
	
		TransakcijeDTO TransactionDTO=new TransakcijeDTO();
		TransactionDTO.setAmount(transacton.getAmount());
		TransactionDTO.setCallNumber(transacton.getCallNumber());
		TransactionDTO.setCreditor(transacton.getCreditor());
		TransactionDTO.setDateOfReceipt(transacton.getDateOfReceipt());
		TransactionDTO.setDateOfValue(transacton.getDateOfValue());
		TransactionDTO.setDebitModel(transacton.getDebitModel());
		TransactionDTO.setDebtor(transacton.getDebtor());
		TransactionDTO.setDebtorAccount(transacton.getDebtorAccount());
		TransactionDTO.setError(transacton.getError());
		TransactionDTO.setId(transacton.getId());
		TransactionDTO.setPurposeOfPayment(transacton.getPurposeOfPayment());

		return 	TransactionDTO;
	}
	
	
	@Override
	public Transakcije  DTOToTransakcije (TransakcijeDTO transakcijeDTO) {

		Transakcije transakcije=new Transakcije();
		transakcije.setAmount(transakcijeDTO.getAmount());
		transakcije.setCallNumber(transakcijeDTO.getCallNumber());
		transakcije.setCreditor(transakcijeDTO.getCreditor());
		transakcije.setDateOfReceipt(transakcijeDTO.getDateOfReceipt());
		transakcije.setDateOfValue(transakcijeDTO.getDateOfValue());
		transakcije.setDebitModel(transakcijeDTO.getDebitModel());
		transakcije.setDebtor(transakcijeDTO.getDebtor());
		transakcije.setDebtorAccount(transakcijeDTO.getDebtorAccount());
		transakcije.setError(transakcijeDTO.getError());
		transakcije.setId(transakcijeDTO.getId());
		transakcije.setPurposeOfPayment(transakcijeDTO.getPurposeOfPayment());
		
		return transakcije;
	}




	@Override
	public List<TransakcijeDTO> findByDebitor(String debitor, String name) {

		List<Transakcije> transactons=transakcijeRepository.findByDebitor(name ,debitor);
		
		System.out.println("TransactionService.getAll()  "+transactons.size());
		List<TransakcijeDTO> transactonsDto=new ArrayList<TransakcijeDTO>();
		
		
		for (Transakcije transacton : transactons) {
			TransakcijeDTO TransactionDTO=new TransakcijeDTO();
			TransactionDTO.setAmount(transacton.getAmount());
			TransactionDTO.setCallNumber(transacton.getCallNumber());
			TransactionDTO.setCreditor(transacton.getCreditor());
			TransactionDTO.setDateOfReceipt(transacton.getDateOfReceipt());
			TransactionDTO.setDateOfValue(transacton.getDateOfValue());
			TransactionDTO.setDebitModel(transacton.getDebitModel());
			TransactionDTO.setDebtor(transacton.getDebtor());
			TransactionDTO.setDebtorAccount(transacton.getDebtorAccount());
			TransactionDTO.setError(transacton.getError());
			TransactionDTO.setId(transacton.getId());
			TransactionDTO.setPurposeOfPayment(transacton.getPurposeOfPayment());
			transactonsDto.add(TransactionDTO);
		}
		System.out.println("TransactionService.getAll()--  "+transactonsDto.size());

		
		return transactonsDto;
	}
	
	@Override
	public List<TransakcijeDTO> findByRecipient(String Recipient, String name) {

		List<Transakcije> transactons=transakcijeRepository.findByRecipient(name ,Recipient);
		System.out.println("TransactionService.findByRecipient()"+transactons.size());
	
		List<TransakcijeDTO> transactonsDto=new ArrayList<TransakcijeDTO>();
		
		
		for (Transakcije transacton : transactons) {
			TransakcijeDTO TransactionDTO=new TransakcijeDTO();
			TransactionDTO.setAmount(transacton.getAmount());
			TransactionDTO.setCallNumber(transacton.getCallNumber());
			TransactionDTO.setCreditor(transacton.getCreditor());
			TransactionDTO.setDateOfReceipt(transacton.getDateOfReceipt());
			TransactionDTO.setDateOfValue(transacton.getDateOfValue());
			TransactionDTO.setDebitModel(transacton.getDebitModel());
			TransactionDTO.setDebtor(transacton.getDebtor());
			TransactionDTO.setDebtorAccount(transacton.getDebtorAccount());
			TransactionDTO.setError(transacton.getError());
			TransactionDTO.setId(transacton.getId());
			TransactionDTO.setPurposeOfPayment(transacton.getPurposeOfPayment());
			transactonsDto.add(TransactionDTO);
		}
		System.out.println("TransactionService-  "+transactonsDto.size());

		
		return transactonsDto;
	}
}
