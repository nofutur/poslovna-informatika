
var date;
var numberOfExchangeRateList;
var appliedBy;
var bank;


$(document).ready(function () {
	

		date= $("#kursnalistaForm [name='date']");
		appliedBy=$("#kursnalistaForm [name='appliedBy']");
//		bank=$("#kursnalistaForm [name='bank']");
		numberOfExchangeRateList=$("#kursnalistaForm [name='numberOfExchangeRateList']");

		bank=$('#bankSelect');


	getBanks();
	
if(getJwtToken()==null){
    window.location.replace('login.html');
}

    getExchangeRateList();
    
    
    $("#addButton").click(function(){
    	
          $("#kursnalistaForm").submit();
       

       var bank1= bankSelect.value;
       var date1= date.val();
       var numberOfExchangeRateList1= numberOfExchangeRateList.val();
       var appliedBy1= appliedBy.val();
       
	   console.log(date1)
      
        
		$.ajax({
			url: "http://localhost:8080/api/exchangeRateList/" + date1 + "/" + numberOfExchangeRateList1 +"/" +appliedBy1+ "/" + bank1,
			type: "POST",
		
			contentType: "application/json",
			datatype: 'json',
			success: function(data) {

                getExchangeRateList();


			},
  		  error: function(data) {
  			  alert('Wrong!')
  		  }
  		});
  	$('#addKL').modal('toggle');
  });
     
    
	
	
	
});

	function checkDate() {
		   var selectedText = document.getElementById('date').value;
		   var selectedText2 = document.getElementById('appliedBy').value;
		   var selectedDate = new Date(selectedText);
		   var selectedDate2 = new Date(selectedText2);
		   var now = new Date();
		   if (selectedDate < now || selectedDate2 < now) {
		    alert("Date must be in the future");
		  }
	}


function getExchangeRateList() { 
    $.ajax({
        url: "http://localhost:8080/api/exchangeRateList"
    }).then(
        function (data) {
            $('#dataTableBody').empty();
            for (i = 0; i < data.length; i++) {
                newRow =
                    "<tr>"
                    + "<td class=\"date\">" + data[i].date + "</td>"
                    + "<td class=\"numberOfExchangeRateList\">" + data[i].numberOfExchangeRateList + "</td>"
                    + "<td class=\"appliedBy\">" + data[i].appliedBy + "</td>"
                    + "<td class=\"bank\">" + data[i].bank.name + "</td>"

					+" <td> <button id='btnDelete' onclick='deleteKursnaLista("+data[i].id+");'> OBRISI </button> </td>"
                    + "</tr>"
                $("#dataTableBody").append(newRow)
            }
        });
 }


function addUserModal(idUser) {

	fetchUser(idUser);
	idUser1=idUser;
	modalAddUser.modal('show');

}


function deleteKursnaLista(id){
	console.log(id);
	if(confirm('da li zelite da obrisete kursnu listu?')){
    	$.ajax({
        	url: "http://localhost:8080/api/exchangeRateList/"+id,
        	type: "DELETE",
        	datatype: 'json',
        	success: function () {
        		   alert('Exchange rate list deleted!')
        		   location.reload();
       		 },
        	error: function () {
            	alert('greska!')
        	}
    	});
	}
}



function getBanks(){
		$.ajax({
			url: "http://localhost:8080/api/banks/",
			type: "GET",
			headers:  createAuthorizationTokenHeader()
		}).then(
			function (data) {
				
				$.each(data, function (i, item) {
					console.log(data[i]);
					$(bankSelect).append($('<option>', {
					
						
						value: data[i].username,
						text: data[i].name
					}));
				});
				
			
				
//				
//				console.log('getBanks')
//				console.log(data);
//				for(i = 0; i < data.length; i++){
//					var x = document.getElementById("bankSelect");
//					var option = document.createElement("option");
//					option.text = data[i].name;
//					option.value = data[i];
//					x.add(option);
//				}
			});
	}
