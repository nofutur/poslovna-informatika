package rs.poslovna_banka_maven.service;

import java.util.List;

import rs.poslovna_banka_maven.dto.BankDTO;
import rs.poslovna_banka_maven.dto.UserDTO;
import rs.poslovna_banka_maven.entity.User;

public interface  UserServiceInterface  {

	
	public UserDTO findByUsername(String username) ;
	public List<UserDTO> findByBankAndLegalEntity(int username, boolean legalEntity) ;
	public UserDTO editUser(UserDTO userDTO) ;
	public UserDTO getUserById(int  id) ;
	public void save(User user);
	public UserDTO save(UserDTO user);
	public void delete(int  id);
	public List<UserDTO> findByBank(BankDTO bank);

}
