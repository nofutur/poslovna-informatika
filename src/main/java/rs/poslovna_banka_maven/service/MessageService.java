package rs.poslovna_banka_maven.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.poslovna_banka_maven.entity.Message;
import rs.poslovna_banka_maven.repository.MessageRepository;

@Service
public class MessageService implements MessageServiceInterface{
	@Autowired
	private MessageRepository messageRepository;
	
	@Override
    public List<Message> getAll() {
		return messageRepository.findAll();
    }
	
	@Override
    public Message get(Integer id) {
    	Optional<Message> m = messageRepository.findById(id);
        return m.get();
    }
	
	@Override
    public Message insert(Message m) {
    	return messageRepository.save(m);
    }
	
	@Override
    public Message edit(Message m) {
        return messageRepository.save(m);
    }
	
	@Override
    public void delete(Integer id) {
		messageRepository.deleteById(id);
    }
}
