package rs.poslovna_banka_maven.service;

import java.util.List;

import rs.poslovna_banka_maven.dto.ExchangeRateListDTO;
import rs.poslovna_banka_maven.entity.CurrencyExchangeRate;
import rs.poslovna_banka_maven.entity.ExchangeRateList;

public interface ExchangeRateListServiceInterface {

	void delete(Integer id);
	ExchangeRateListDTO save(ExchangeRateListDTO erl);
	List<ExchangeRateListDTO> findAll();
	ExchangeRateListDTO getOne(Integer id);
	ExchangeRateList get(Integer id);
	
}
