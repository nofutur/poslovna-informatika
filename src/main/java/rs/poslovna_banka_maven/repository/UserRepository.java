package rs.poslovna_banka_maven.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import rs.poslovna_banka_maven.entity.Bank;
import rs.poslovna_banka_maven.entity.User;


public interface UserRepository extends JpaRepository<User, Integer> {
	@EntityGraph(attributePaths = "authorities")
    Optional<User> findOneWithAuthoritiesByUsername(String username);
	User findByUsername(String username);
	User findByFirstname(String username);

	@Query(value = "Select * from users u where u.user_id in "
			+ "(select user_id from bank_accounts ba where ba.bank_id = ?)"
			+ " and u.user_legal_entity = ?", nativeQuery = true)
	List<User> findByBankAndLegalEntity(int bankId, boolean legalEntity);
}
