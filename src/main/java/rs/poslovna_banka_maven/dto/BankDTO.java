package rs.poslovna_banka_maven.dto;

import java.util.ArrayList;
import java.util.List;

import rs.poslovna_banka_maven.entity.Bank;
import rs.poslovna_banka_maven.entity.BankAccount;
import rs.poslovna_banka_maven.entity.ExchangeRateList;

public class BankDTO {

	private int id;
	private String username;
	private boolean activated;
	private String code;
	private String swiftCode;
	private String accountingBill;
	private int TIN;
	private String name;
	private String address;
	private String web;
	private String phone;
	private String fax;
	private double balance;
	public List<BankAccountDTO> bankAccounts = new ArrayList<BankAccountDTO>();
	public List<ExchangeRateListDTO> erl = new ArrayList<ExchangeRateListDTO>();
	
	
	public BankDTO() {
		super();
	}
	
	public BankDTO(Bank bank) {
			super();
			this.id = bank.getId();
			this.username = bank.getUsername();
			this.activated = bank.isActivated();
			this.code = bank.getCode();
			this.swiftCode = bank.getSwiftCode();
			this.accountingBill = bank.getAccountingBill();
			this.TIN = bank.getTIN();
			this.name = bank.getName();
			this.address = bank.getAddress();
			this.web = bank.getWeb();
			this.phone = bank.getPhone();
			this.fax = bank.getFax();
			this.balance = bank.getBalance();
			ArrayList<BankAccountDTO> accounts = new ArrayList<BankAccountDTO>();
			for(BankAccount acc : bank.getBankAccounts()) {
				accounts.add(new BankAccountDTO(acc));
			}
			this.bankAccounts = accounts;
			ArrayList<ExchangeRateListDTO> erl = new ArrayList<ExchangeRateListDTO>();
			for(ExchangeRateList e : bank.getErl()) {
				erl.add(new ExchangeRateListDTO(e));
			}
			this.erl = erl;
		}
	
	
	public BankDTO(int id, String username, boolean activated, String code, String swiftCode, String accountingBill,
		int tIN, String name, String address, String web, String phone, String fax, double balance,
		List<BankAccountDTO> bankAccounts, List<ExchangeRateListDTO> erl) {
		super();
		this.id = id;
		this.username = username;
		this.activated = activated;
		this.code = code;
		this.swiftCode = swiftCode;
		this.accountingBill = accountingBill;
		TIN = tIN;
		this.name = name;
		this.address = address;
		this.web = web;
		this.phone = phone;
		this.fax = fax;
		this.balance = balance;
		this.bankAccounts = bankAccounts;
		this.erl = erl;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public boolean isActivated() {
		return activated;
	}
	public void setActivated(boolean activated) {
		this.activated = activated;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getSwiftCode() {
		return swiftCode;
	}
	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}
	public String getAccountingBill() {
		return accountingBill;
	}
	public void setAccountingBill(String accountingBill) {
		this.accountingBill = accountingBill;
	}
	public int getTIN() {
		return TIN;
	}
	public void setTIN(int tIN) {
		TIN = tIN;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getWeb() {
		return web;
	}
	public void setWeb(String web) {
		this.web = web;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public List<BankAccountDTO> getBankAccounts() {
		return bankAccounts;
	}
	public void setBankAccounts(List<BankAccountDTO> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}
	public List<ExchangeRateListDTO> getErl() {
		return erl;
	}
	public void setErl(List<ExchangeRateListDTO> erl) {
		this.erl = erl;
	}	
}
